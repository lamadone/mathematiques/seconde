\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother


\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}

\newtheorem{preuve}{Preuve}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Vecteurs}
\author{J.B.S. 2\up{nde}}
\date{}

\begin{document}

\maketitle

\section*{Introduction}

La géométrie vectorielle voit sa naissance à l'aube du XVIIème siècle,
lors de la mathématisation de la physique : on commence à parler du
vecteur vitesse ou du vecteur déplacement du centre de gravité d'un
solide.

Les vecteurs sont vite adoptés comme «outil mathématique» permettant de
décrire les interactions, les forces et autres. Ainsi le principe
fondamental de la statique ou de la dynamique s'expriment très aisément
vectoriellement.

Le XIXième siècle est l'occasion de systématiser, de généraliser et de
voir le liens qu'entretiennent les vecteurs avec la géométrie.

Ainsi, on se permet d'imaginer des vecteurs à plus de 3 coordonnées (on
parle aussi de dimension), de voir qu'est ce qui leur confère leurs
propriétés intrinsèques et de développer d'autres «outils» : le calcul
matriciel.

\section{Le vecteur, expression d'un déplacement}

\begin{definition}
  Soit $A$ et $B$ un couple de point de l'espace euclidien réel. On
  admet qu'il existe une unique translation, notée $t_{AB}$ qui
  transforme le point $A$ en un point $B$ donné.

  Cette unique translation définit ainsi, le vecteur $\Vecteur{AB}$.
\end{definition}

On peut répeter cette translation ou la faire partir d'un autre point
arbitraire du plan. Avant de définir l'égalité de vecteurs ou l'égalité
des translations associées, caractérisons un vecteur.

Puisqu'on l'a déjà vu, un vecteur est caratérisé par deux points.
Ceux-ci définissent une droite et le segment de droite qui joint $A$ et
$B$ possède une certaine longueur.

\begin{definition}
  On définit, pour un vecteur $\Vecteur{AB}$ donné, les trois
  informations suivantes :
  \begin{itemize}
    \item direction -- c'est la droite $(AB)$ qui porte le vecteur ;
    \item sens -- pour le vecteur précédent, de $A$ vers $B$ ;
    \item norme -- c'est la «longueur» du vecteur.
  \end{itemize}
  On note cette dernière quantité $\lVert\Vecteur{AB}\rVert$.
\end{definition}

\begin{propriete}[égalité de deux vecteurs]
  Deux vecteurs sont égaux si et seulement si ils définissent le même
  déplacement
\end{propriete}

Une conséquence immédiate en utilisant les «quantités» définies en 2 :
\begin{propriete}
  Deux vecteurs sont égaux si et seulement si ils ont :
  \begin{itemize}
    \item même direction ;
    \item même sens ;
    \item même norme.
  \end{itemize}
\end{propriete}

Interprétation géométrique de l'égalité de deux vecteurs qui dérive de
ce qui précède :
\begin{propriete}
  Deux vecteurs $\Vecteur{AB}$ et $\Vecteur{CD}$ sont égaux si et
  seulement si $ABDC$ est un parallélogramme.
\end{propriete}

\begin{center}
  \begin{tikzpicture}[yscale=0.7]
    \draw[->] (0,0) node[below left] {$A$} -- (7,4) node[right] {$B$} ;
    \draw[->,xshift=-50,yshift=30] (0,0) node[left] {$C$} -- (7,4)
    node[above right] {$D$} ;
    \draw[dashed] (0,0) -- (0-50pt,0+30pt) ;
    \draw[dashed] (7,4) -- (7cm-50pt,4cm+30pt) ;
  \end{tikzpicture}
\end{center}

\begin{preuve}
  En utilisant la caractérisation des vecteurs : Deux vecteurs sont
  égaux si et seulement s'ils sont portés par des droites parallèles, de
  même sens et de même longueur. La figure ainsi formé comporte deux
  droites parallèles supportant des segments de même longueurs, c'est
  donc un parallélogramme, la réciproque étant triviale.
\end{preuve}

\begin{definition}[représentant]
  Lorsqu'on choisit un vecteur particulier pour parler de différents
  vecteurs égaux, on le note $\vec{u}$. On parle de représentant.
\end{definition}

On peut également définir le vecteur «réciproque» d'un vecteur donné,
comme étant le vecteur qui ramène au point de départ. Ainsi, si un
vecteur est défini par une translation de $A$ vers $B$, nous pouvons
définir le vecteur $\Vecteur{BA}$ translation de $B$ vers $A$.

\begin{definition}[notation du vecteur opposé]
  Le vecteur opposé à $\Vecteur{AB}$ est noté $\Vecteur{BA} =
  -\Vecteur{AB}$
\end{definition}

\begin{propriete}[caractérisation du milieu d'un segment]
  $I$ est le milieu du segment $[AB]$ si et seulement si $\Vecteur{AI} =
  \Vecteur{IB}$
\end{propriete}

\section{Opérations sur les vecteurs}

On va ici tâcher de définir des opérations sur des vecteurs : étant
donné deux vecteurs $\vec{u}$ et $\vec{v}$, quelles sont les opérations
possibles sur des vecteurs et comment les effectuer.

Avant cela, quelques définitions :
\begin{definition}[vecteur nul]
  On admet l'existence d'un vecteur nul, correspondant à la translation
  qui déplace le point $A$ sur lui-même.

  On note $\Vecteur{AA} = \vec{0}$.
\end{definition}

\subsection{Somme de vecteurs}

\begin{definition}[somme de vecteurs]
  Soit $\vec{u}$ et $\vec{v}$ deux vecteurs du plan. On définit la somme
  de deux vecteurs comme le déplacement équivalent à ces deux
  déplacements successifs.

  Géométriquement, la construction est comme il suit :
  \begin{center}
    \begin{tikzpicture}
      \draw [->] (0,0) -- (2,1) ;
      \draw [->,shift={(-0.5,-1)}] (0,0) -- (3,-2) ;
      \draw [->,dashed] (0,0) -- (3,-2) ;
      \draw [->,dashed,shift={(2,1)}] (0,0) -- (3,-2) ;
      \draw [->,thick] (0,0) -- (2+3,1+-2) ;
      \draw (1,0.5) node [above] {$\vec{u}$} ;
      \draw [shift={(-0.5,-1)}] (1.5,-1) node [above] {$\vec{v}$} ;
      \draw ({(2+3)/2},{(1+-2)/2}) node [above] {$\vec{u}+\vec{v}$} ;
    \end{tikzpicture}
  \end{center}
\end{definition}

Le protocole de construction est le suivant :
\begin{itemize}
  \item tracer la droite parallèle à la droite portant $\vec{v}$ passant
    par l'extrémité du vecteur $\vec{u}$ ;
  \item reporter la longueur (norme) du vecteur $\vec{v}$ sur cette
    droite ;
  \item le point ainsi obtenu est l'extrémité d'arrivée du vecteur somme
    $\vec{u}+\vec{v}$ ;
  \item le vecteur somme est le vecteur qui part du point de départ de
    $\vec{u}$ et qui va à l'extrémité obtenue à l'étape précédente.
\end{itemize}

\begin{propriete}
  La somme de vecteurs est :
  \begin{itemize}
    \item associative ;
    \item commutative.
  \end{itemize}
\end{propriete}
Ces propriétés se lisent mathématiquement de la façon suivante :

$\forall (\vec{u},\vec{v})\in E^2,\ \vec{u} + \vec{v} = \vec{v} +
\vec{v}$.

$\forall (\vec{u},\vec{v}, \vec{w})\in E^3,\ \vec{u} + (\vec{v} +
\vec{w} ) = (\vec{u} + \vec{v} ) + \vec{w}$.

On note souvent cette dernière somme $\vec{u} + \vec{v} + \vec{w}$ sans
parenthèses.

On admet la relation suivante, dite relation de Chasles :
\begin{propriete}[relation de Chasles]
  Pour tout vecteur $\Vecteur{AB}$ et pour tout point $C$, on a
  l'égalité suivante : \[ \Vecteur{AB} = \Vecteur{AC} + \Vecteur{CB} \]
\end{propriete}

Cette relation de Chasles est à la base de nombreux raisonnements et de
nombreuses démonstrations.

Elle s'illustre de la façon suivante :
\begin{center}
  \begin{tikzpicture}
    \draw (0,0) node [below] {$A$} ;
    \draw (2,3) node [right] {$B$} ;
    \draw (4,2) node [above right] {$C$} ;
    \draw (-3,0) node [left] {$C'$} ;

    \draw [->] (0,0) -- (2,3) ;
    \draw [->] (0,0) -- (4,2) ;
    \draw [->] (4,2) -- (2,3) ;

    \draw [->] (0,0) -- (-3,0) ;
    \draw [->] (-3,0) -- (2,3) ;
  \end{tikzpicture}
\end{center}

\subsection{Différence de vecteurs}

Pour la différence de deux vecteurs, on exploite cette règle élaborée
pour la différence des nombres relatifs : \emph{Soustraire un nombre,
c'est additionner son opposé}.

Ainsi, $\vec{u} - \vec{v} = \vec{u} + - \vec{v} $.

\begin{center}
  \begin{tikzpicture}
    \draw [->] (0,0) -- (2,1) ;
    \draw [->,shift={(-0.5,-1)}] (0,0) -- (3,-2) ;
    \draw [->,dashed] (0,0) -- (3,-2) ;
    \draw [->,dashed,shift={(2,1)}] (0,0) -- (3,-2) ;
    \draw [->,thick,shift={(3,-2)}] (0,0) -- (2-3,1--2) ;
    \draw (1,0.5) node [above] {$\vec{u}$} ;
    \draw [shift={(-0.5,-1)}] (1.5,-1) node [above] {$\vec{v}$} ;
    \draw ({(2+3)/2},{(1+-2)/2}) node [above] {$\vec{u}-\vec{v}$} ;
  \end{tikzpicture}
\end{center}

\subsection{Produit d'un vecteur par un nombre réel}

Commençons par le produit d'un vecteur par un nombre entier positif : il
s'agit simplement, de reporter, dans la même direction et dans le même
sens la longueur (ou norme du vecteur).
\begin{center}
  \begin{tikzpicture}
    \draw [thick,->] (0,0) -- (2,1) ;
    \draw [->,shift={(2,1)}] (0,0) -- (2,1) ;
  \end{tikzpicture}
\end{center}

On peut, assez facilement, définir le produit d'un vecteur par un entier
négatif : on reporte le vecteur autant de fois que nécessaire, mais dans
la direction opposée.
\begin{center}
  \begin{tikzpicture}
    \draw [thick,->] (0,0) -- (2,1) ;
    \draw [->,shift={(0,0)}] (0,0) -- (-2*2,-1*2) ;
  \end{tikzpicture}
\end{center}

On peut étendre ces règles à la définition suivante :
\begin{definition}[produit d'un vecteur par un réel]
  Soit $\vec{u}$ un vecteur du plan et $k$ un nombre réel.
  \begin{itemize}
    \item si $k>0$, $k\vec{u}$ est un vecteur de même direction et même
      sens que $\vec{u}$, et de norme $k\lVert\vec{u}\rVert$ ;
    \item si $k<0$, $k\vec{u}$ est un vecteur de même direction que
      $\vec{u}$, de sens opposé à  $\vec{u}$, et de norme
      $-k\lVert\vec{u}\rVert$ ;
    \item si $k=0$, $k\vec{u} = \vec{0}$
  \end{itemize}
\end{definition}

On admet les règles opératoires suivantes :
\begin{propriete}[distributivité des scalaires et des vecteurs]
  \begin{itemize}
    \item $\forall (k,k')\in\mathbf{R}^2$ et $\vec{u}$ un vecteur,
      $(k+k')\vec{u} = k\vec{u} + k'\vec{v}$
    \item $\forall k\in\mathbf{R}$ et $\vec{u}$ et $\vec{v}$ deux
      vecteurs, $k(\vec{u} + \vec{v}) = k\vec{u} + k\vec{v}$.
  \end{itemize}
\end{propriete}

Ces opérations permettent de définir une nouvelle notion riche en
implication, qui constitue la section suivante.

\section{Colinéarité, bases et coordonnées}

\subsection{Colinéarité}

\begin{definition}[Colinéarité]
  On dit que deux vecteurs $\vec{u}$ et $\vec{v}$ sont colinéaires si
  et seulement si il existe $k\in\mathbf{R^*}$ tel que $\vec{v} =
  k\vec{u}$ ou $\vec{u} = k\vec{v}$.
\end{definition}

\begin{propriete}[une autre formulation de la colinéarité]
  La définition précédente est équivalente à $\vec{u}$ et $\vec{v}$ sont
  colinéaires s'il existe un couple de nombres réels différents de
  $(0,0)$ tel que $a\vec{u} + b\vec{v} = \vec{0}$.
\end{propriete}

\begin{remarque}
  On dit aussi que $\vec{u}$ et $\vec{v}$ sont liés.
\end{remarque}

\begin{propriete}[colinéarité en terme de droites]
  Deux vecteurs sont colinéaires si et seulement s'ils ont même
  direction (ou sont portés par des droites parallèles).
\end{propriete}

\begin{remarque}
  Dans l'espace à deux dimensions, on admet que :
  \begin{itemize}
    \item toute famille de plus de 3 vecteurs est nécessairement liée ;
    \item tout vecteur du plan est combinaison linéaire de deux vecteurs
      non colinéaires (c'est une conséquence de la relation de Chasles) ;
    \item une famille de deux vecteurs non colinéaires s'appelle une
      famille génératrice ; dans l'espace de dimension 2, on parle
      également de \emph{base}.
  \end{itemize}
\end{remarque}


\begin{center}
  \begin{tikzpicture}
    \foreach \x in {-3,...,4}
    {
      \foreach \y in {-3,...,5}
      {
        \begin{scope}[very thin,
          xshift=\x cm + 2/3*\y cm,yshift=\y cm + -0.5*\x cm]
          \draw (0,0) -- (1,-0.5) ;
          \draw (0,0) -- (2/3,1) ;
        \end{scope}
      }
    }
    \draw [very thin] (-3,-3) grid (6,5) ;

    \draw [red,thick,->] (0,0) -- (1,-0.5) node [below right] {$B$} ;
    \draw [red,thick,->] (0,0) -- (2/3,1) node [above] {$C$} ;
    \draw [blue,thick,->] (0,0) -- (1,0) node [below right] {$I$};
    \draw [blue,thick,->] (0,0) -- (0,1) node [above left] {$J$} ;

    \draw [->,green!40!black] (0,0) -- (4,2) node [above right] {$M$} ;
    \draw (0,0) node [below left] {$A$} ;
  \end{tikzpicture}
\end{center}

Dans cette figure, $\Vecteur{AM} = 4\Vecteur{AI} + 2\Vecteur{AJ} =
2\Vecteur{AB} + 3\Vecteur{AC}$

\subsection{Coordonnées dans une base}

\begin{definition}[coordonnées d'un vecteur dans une base]
  Soit $\vec{w}$ un vecteur dont l'expression dans la base $(\vec{u} ;
  \vec{v})$ est $\vec{w} = \alpha\vec{u} + \beta\vec{v}$.

  On dit alors que $\alpha$ et $\beta$ sont les coordonnées de $\vec{w}$
  dans la base $(\vec{u} ;\vec{v})$ et on note \[ \vec{w} =
  \left(\begin{array}{c}\alpha \\ \beta\end{array}\right) \] s'il n'y a
  pas d'ambiguité. Sinon, on précise la base : \[ \vec{w} =
     \left(\begin{array}{c}\alpha \\ \beta\end{array}\right)_{(\vec{u} ;
  \vec{v})} \]

\end{definition}

\begin{remarque}
  $(\vec{u}; \vec{v})$ étant libre, les coefficients $\alpha$ et $\beta$
  sont uniques dans la base choisie. Ainsi $\vec{AM}$ a pour coordonnées
  $\left(\begin{matrix}4 \\ 2\end{matrix}\right)$ dans la base $(\vec{AI}
  ; \vec{AJ})$ et $\left(\begin{matrix}2 \\ 3\end{matrix}\right)$ dans la
  base $(\vec{AB} ; \vec{AC})$.
\end{remarque}

Ainsi défini, un vecteur $\Vecteur{AB}$ peut s'exprimer dans la base
$(\vec{u}; \vec{v})$ : $\Vecteur{AB} = \alpha\vec{u} + \alpha\vec{v}$.
On peut aussi définir les coordonnées d'un point dans un repère.

\begin{definition}[repère]
  Un triplet $(A;\vec{u};\vec{v})$ où $(\vec{u};\vec{v})$ est une base
  s'appelle un repère.
\end{definition}

\begin{propdef}
  ~\\[-5mm]
  Si $\Vecteur{AB} = \alpha\vec{u} + \beta\vec{v}$ dans la base
  $(\vec{u}; \vec{v})$, on dit que $\alpha$ et $\beta$ sont les
  coordonnées du point $B$ dans le repère $(A;\vec{u};\vec{v})$ où
  $(\vec{u};\vec{v})$. On note \[ B = \left(\begin{array}{c}\alpha \\
  \beta\end{array}\right)_{(A; \vec{u} ; \vec{v})} \]
\end{propdef}

\begin{remarque}
  Comme les coordonnées de $A$ dans le repère $(A;\vec{u};\vec{v})$ sont
$\left(\begin{array}{c} 0 \\ 0 \end{array}\right)_{(A; \vec{u} ;
  \vec{v})}$, on dit que $A$ est l'origine du repère.
\end{remarque}

\begin{propriete}[calcul des coordonnées d'un vecteur dans une base]
  ~\\[-5mm]
  Si $B = \left(\begin{array}{c}\alpha_B \\ \beta_B\end{array}\right)_{
    (A; \vec{u} ; \vec{v})}$ et $C = \left(\begin{array}{c}\alpha_C \\
    \beta_C\end{array}\right)_{(A; \vec{u} ; \vec{v})}$, alors \[
      \Vecteur{BC} = \left(\begin{array}{c}\alpha_C - \alpha_B \\
    \beta_C - \beta_B \end{array}\right)_{(\vec{u} ; \vec{v})}\]
  \end{propriete}

\begin{propriete}[calcul des coordonnées de la somme de deux vecteurs]
  ~\\[-5mm]
  Si $\vec{r} = \left(\begin{array}{c}\alpha_r \\
  \beta_r\end{array}\right)_{( \vec{u} ; \vec{v})}$ et $\vec{s} =
  \left(\begin{array}{c}\alpha_s \\
  \beta_s\end{array}\right)_{(\vec{u} ; \vec{v})}$, alors \[
    \vec{r} + \vec{s} = \left(\begin{array}{c}\alpha_r + \alpha_s \\
    \beta_r + \beta_s \end{array}\right)_{(\vec{u} ; \vec{v})}\]
\end{propriete}

\begin{propriete}[égalité de vecteurs]
  Deux vecteurs sont égaux si et seulement s'ils ont les mêmes
  coordonnées dans une même base.
\end{propriete}

On peut aussi démontrer, en utilisant l'équivalence $I$ milieu de $[AB]
\iff \Vecteur{AI} = \frac12\Vecteur{AB}$ la propriété suivante sur les
coordonnées du milieu d'un segment.

\begin{propriete}[coordonnées du milieu d'un segment]
  Soient $A$ et $B$ deux points de coordonnées $(x_A, y_A),\ (x_B, y_B)$
  dans un repère donné.

  Alors les coordonnées du milieu $I$ du segment $[AB]$ sont \[
    \left(\begin{array}{c}\frac{x_1 + x_2}2 \\ \frac{y_1 + y_2}2
  \end{array}\right) \]
\end{propriete}

\section{Norme d'un vecteur}

\subsection{Repères privilégiés}

Dans une base donnée, on peut imposer que les vecteurs de celle-ci
soient de norme 1.

On peut également imposer qu'ils soient orthogonaux.

Lorsque ces deux conditions sont remplies, on parle d'un repère
orthonormé (et d'une base orthonormale.)

\subsection{Théorème de Pythagore}

On rappelle ici sans démontrer l'égalité dite égalité de Pythagore qui
n'est vraie que si le triangle considéré possède un angle droit :

\begin{center}
  \begin{tikzpicture}
    \draw (0,0) -- (3,0) -- (3,4) -- cycle ;
    \draw (3,0) -- (3-0.1,0) -- (3-0.1,0+0.1) -- (3,0+0.1) -- cycle ;
    \draw (0,0) node [below left] {$A$} ;
    \draw (3,0) node [below right] {$B$} ;
    \draw (3,4) node [above right] {$C$} ;
    \draw [dashed,->,very thick] (0,0) -- (0,1) ;
    \draw [dashed,->,very thick] (0,0) -- (1,0) ;
  \end{tikzpicture}
  \[ AB^2 + BC^2 = AC^2 \]
\end{center}

Si on pose que $\lVert\vec{u}\rVert = 1$, on obtient $AB =
\lVert\Vecteur{AB}\rVert = \lvert x_{AB} \rvert$.

De même, en posant $\lVert{v} = 1$, on obtient que $BC =
\lVert\Vecteur{BC}\rVert = \lvert y_{BC} \rvert$.

En utilisant le théorème de Pythagore, on en déduit le cas général,
lorsqu'on considère que les vecteurs $\vec{u}$ et $\vec{v}$ sont
orthogonaux : \[ \lVert\Vecteur{AC}\rVert^2 = x_{AB}^2 + y_{BC}^2 \]

Or $x_{AB} = x_{AC}$ et $y_{BC} = y_{AC}$. On peut donc définir, dans le
cas général une formule de calcul de la norme.

\subsection{Formule de calcul de la norme}

\begin{definition}[Norme d'un vecteur]
  On définit la norme d'un vecteur $\Vecteur{AB}$ par la formule
  suivante, conséquence du théorème de Pythagore : \[ \lVert
  \Vecteur{AB} \rVert = \sqrt{x_{AB}^2 + y_{AB}^2} . \]
\end{definition}

On vérifie aisément le respect de la propriété multiplicative de la
norme.

\begin{propriete}[multiplication par un scalaire]
  Soient $\vec{u}$ un vecteur de norme $\lVert\vec{u}\rVert$ et
  $k\in\R$.

  Alors $\lVert k \vec{u} \rVert = \lvert k \rvert \times \rVert \vec{u}
  \lVert$
\end{propriete}

\begin{propriete}[distance entre deux points]
  La distance entre deux points $A$ et $B$ est la norme du vecteur
  $\Vecteur{AB}$.
\end{propriete}

\begin{propriete}[inégalité triangulaire]
  Soient $A$, $B$ et $C$ trois points du plan,
  \[ \lVert \Vecteur{AB} + \Vecteur{BC} \rVert \leq \lVert \Vecteur{AB}
  \rVert + \lVert \Vecteur{BC} \rVert \] avec égalité si et seulement si
  les points sont alignés.
\end{propriete}

\end{document}
