import os
import time

# Set the directory path
directory = '.'

# Initialize an empty list to store file information
files = []

# Walk through the directory and gather file information
for root, _, filenames in os.walk(directory):
    for filename in filenames:
        filepath = os.path.join(root, filename)
        files.append({
            'name': filename,
            'path': filepath,
            'size': os.path.getsize(filepath),
            'modified': os.path.getmtime(filepath)
        })

# Sort files by name
files.sort(key=lambda x: x['name'])

# Generate the HTML table
html = '<table>\n'
html += '<tr><th>Name</th><th>Path</th><th>Size (bytes)</th><th>Modified</th></tr>\n'
for file in files:
    html += '<tr><td>{}</td><td><a href="{}">{}</a></td><td>{}</td><td>{}</td></tr>\n'.format(
        file['name'],
        file['path'][2:],
        file['path'][2:],
        file['size'],
        time.ctime(file['modified'])
    )
html += '</table>'

# Save the HTML to a file
with open('index.html', 'w') as f:
    f.write(html)
