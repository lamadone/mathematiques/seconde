\chapter{Généralités sur les fonctions}

\section{Vocabulaire des fonctions et exemples}

\subsection{Vocabulaire}

\begin{definition}
	Une fonction est un «objet mathématique» qui, à tout élément d'un ensemble de départ, associe un \textbf{unique} élément d'un ensemble d'arrivée.
\end{definition}

\begin{exemple}
	La fonction \texttt{ord} associe à tout caractère un nombre unique.
\end{exemple}

\begin{definition}
	Une fonction numérique réelle est une fonction qui, à tout élément $x \in D \subset \R$, associe un unique réel $y$ appelé \textbf{image} de $x$ par $f$.\\
	L'ensemble des couples $(x;y=f(x))$ forme le graphe de la fonction et s'ils sont tracés dans un repère, on parle de \textbf{courbe représentative}.
\end{definition}

\begin{note}
	\begin{itemize}
		\item La fonction est représentée par une lettre, comme $f$.
		\item L'image du nombre par la fonction est noté $f(x)$ («$f$ de $x$»). C'est un nombre.
		\item On note parfois $f \colon D \to \R,\ x \mapsto f(x)$.
		\item Si, pour $b$ fixé, il existe $a$ tel que $f(a) = b$, on dit que $a$ est un antécédent de $b$ par $f$.
	\end{itemize}
\end{note}

\subsection{Exemples}
\opencutright
\renewcommand*{\windowpagestuff}{%
	\centering
	\begin{tikzpicture}[>=latex]
		\draw [blue!25] (-1,-1) grid (6,4) ;
		\draw [thick,->] (-1,0) -- (6,0) ;
		\draw [thick,->] (0,-1) -- (0,4) ;
		\foreach \i in {1,2,5} { \draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; }
		\foreach \i in {1,2,3} { \draw (0.1,\i) -- (-0.1,\i) node [left] {$\i$} ; }
		\draw [very thick,green!50!black] (1,0) node {$[$} -- (5,0) node {$]$} ;
		\draw [very thick,green!50!black] (3.5,0) node [below] {$D$} ;
		
		\draw [dashed,thick] (0,3) -- (2,3) -- (2,0) ;
		
		\draw [red,thick] plot [smooth,domain=1:3] 
		(\x,{2-(\x-1)*(\x-3)});
		\draw [red,thick] plot [smooth,domain=3:4] 
		(\x,{2+(\x-3)*(\x-5)});
		\draw [red,thick] plot [smooth,domain=4:5] 
		(\x,{3+2*(\x-3)*(\x-5)});
		\draw (1,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (2,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (3,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (4,1) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (5,3) node [cross=3pt,rotate=45,very thick,red] {} ;
	\end{tikzpicture}
}
\begin{exemple}[Avec une courbe]
		~\\[7pt]
		\begin{cutout}{0}{\dimexpr\linewidth-8.5cm\relax}{0pt}{6}
			\noindent La courbe tracée en rouge définit une fonction sur l'intervalle $D = \intv{1}{5}$. L'image de 2 par $f$ est 3.\\
			Attention, seuls les points repérés sur le graphique (croix ou lignes pointillés) sont des valeurs exactes.
		\end{cutout}
\end{exemple}

\pagebreak
\renewcommand*{\windowpagestuff}{%
	\centering
	\begin{tabular}{|c|c|}\hline
		$x$ & $f(x)$ \\ \hline
		1 & 0,1 \\ \hline
		1,5 & 0,2 \\ \hline
		2 & 0,4 \\ \hline
		3 & -0,2 \\ \hline
	\end{tabular}
}
\begin{exemple}[Avec un tableau]
	~\\[7pt]
	\begin{cutout}{0}{\dimexpr\linewidth-8.5cm\relax}{0pt}{6}
		\noindent Attention, la fonction $f$ ici n'est explicitement définie que pour les valeurs présentes dans le tableau. Ainsi, la fonction n'est pas définie en 2,5.
	\end{cutout}
\end{exemple}

\begin{exemple}[Avec une expression explicite]
	~\\[7pt]
	La fonction $f \colon \intv{-2}{4} \to \R,\ x \mapsto x^2 - 3x + 5$ est définie ici par une formule. On écrit aussi «$f$ est la fonction qui, à tout $x$ compris entre $-2$ et $4$, associe $f(x) = x^2 - 3x + 5$.\\
	Pour calculer la valeur de $f(2)$, on remplace $x$ par $2$ : $f(2) = 2^2 - 3×2 + 5 = 4 - 6 + 5 = 3$.
\end{exemple}

\section{Représentation graphique et applications}

\subsection{Représentation graphique}

\begin{definition}
	On appelle représentation graphique de la fonction $f$ l'ensemble des points de coordonnées $(x;f(x))$, pour $x$ appartenant à $D$ l'intervalle de définition de la fonction.
\end{definition}

\begin{proposition}
	$M: (x;y)$ appartient à $\mathscr{C}_f$ si et seulement si $x \in D$ et $y = f(x)$.
\end{proposition}

\subsection{Résolution des équations $f(x) = k$ et $f(x) = g(x)$}

\begin{proposition}
	Les solutions de l'équation $f(x) = k$ sont les abscisses des points où la courbe $\mathscr{C}_f$ coupe la droite d'équation $y = k$.
\end{proposition}

\begin{exemple}
	~\\[7pt]
	\begin{center}
		\begin{tikzpicture}[>=latex]
			\draw [blue!25] (-1,-1) grid (6,4) ;
			\draw [thick,->] (-1,0) -- (6,0) ;
			\draw [thick,->] (0,-1) -- (0,4) ;
			\foreach \i in {1,2,3,4,5} { \draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; }
			\foreach \i in {1,2,3} { \draw (0.1,\i) -- (-0.1,\i) node [left] {$\i$} ; }

			\draw [green!50!black,very thick] (0,2) -- (6,2) ;
			\draw [green!50!black,thick,dashed] (1,2) -- (1,0) ;
			\draw [green!50!black,thick,dashed] (3,2) -- (3,0) ;
			\draw [green!50!black,thick,dashed] (4.71,2) -- (4.71,0) ;
			
			\draw [red,thick] plot [smooth,domain=1:3] 
			(\x,{2-(\x-1)*(\x-3)});
			\draw [red,thick] plot [smooth,domain=3:4] 
			(\x,{2+(\x-3)*(\x-5)});
			\draw [red,thick] plot [smooth,domain=4:5] 
			(\x,{3+2*(\x-3)*(\x-5)});
			\draw (1,2) node [cross=3pt,rotate=45,very thick,red] {} ;
			\draw (2,3) node [cross=3pt,rotate=45,very thick,red] {} ;
			\draw (3,2) node [cross=3pt,rotate=45,very thick,red] {} ;
			\draw (4,1) node [cross=3pt,rotate=45,very thick,red] {} ;
			\draw (5,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\end{tikzpicture}
	\end{center}
	Les solutions de l'équation $f(x) = 2$ sont $1; 3$ et $a \approx 4,71$. On note $S = \set{1;3;a}$, où $a \in \intv{4}{5}$. Pour obtenir une information plus précise, il faudrait une graduation plus précise.
\end{exemple}

\begin{proposition}
	Les solutions de l'équation $f(x) = g(x)$ sont les abscisses des points où les courbe $\mathscr{C}_f$ et $\mathscr{C}_g$ se coupent.
\end{proposition}

\begin{exemple}
	~\\[7pt]
	\begin{center}
		\begin{tikzpicture}[>=latex]
		\draw [blue!25] (-1,-1) grid (7,5) ;
		\draw [thick,->] (-1,0) -- (7,0) ;
		\draw [thick,->] (0,-1) -- (0,5) ;
		\foreach \i in {1,2,3,4,5,6} { \draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; }
		\foreach \i in {1,2,3,4} { \draw (0.1,\i) -- (-0.1,\i) node [left] {$\i$} ; }
		
		\draw [red,thick] plot [smooth,domain=1:3] 
		(\x,{2-(\x-1)*(\x-3)});
		\draw [red,thick] plot [smooth,domain=3:4] 
		(\x,{2+(\x-3)*(\x-5)});
		\draw [red,thick] plot [smooth,domain=4:5.25] 
		(\x,{3+2*(\x-3)*(\x-5)});
		\draw (1,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (2,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (3,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (4,1) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (5,3) node [cross=3pt,rotate=45,very thick,red] {} ;

		\draw [green!50!black,thick] plot [smooth,domain=1:6]
		(\x,{3-0.5*(\x-2)*(\x-5)});

		\end{tikzpicture}
	\end{center}
	Les solutions de \tikz[anchor=base, baseline]{\node [red] {$f(x)$ } }$=$\tikz[anchor=base, baseline]{\node [green!50!black] {$g(x)$ } } sont $x = 2$ et $x = 5$.
\end{exemple}

\subsection{Résolution des inéquations $f(x) < k$ et $f(x) < g(x)$}

\begin{proposition}
	Les solutions de l'inéquation $f(x) < k$ sont les abscisses des points où la courbe $\mathscr{C}_f$ est sous la droite d'équation $y = k$.
\end{proposition}

\begin{exemple}
	~\\[7pt]
	\begin{center}
		\begin{tikzpicture}[>=latex]
		\draw [blue!25] (-1,-1) grid (6,4) ;
		\draw [thick,->] (-1,0) -- (6,0) ;
		\draw [thick,->] (0,-1) -- (0,4) ;
		\foreach \i in {1,2,3,5} { \draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; }
		\foreach \i in {1,2,3} { \draw (0.1,\i) -- (-0.1,\i) node [left] {$\i$} ; }
		
		\draw [green!50!black,thick] (0,2) -- (6,2) ;
		\draw [green!50!black,thick,dashed] (3,2) -- (3,0) ;
		\draw [green!50!black,thick,dashed] (4.71,2) -- (4.71,0) ;
		\draw [very thick,green!50!black] (3,0) node {$]$} -- (4.71,0) node {$[$} ;
		
		\draw [red,thick] plot [smooth,domain=1:3] 
		(\x,{2-(\x-1)*(\x-3)});
		\draw [red,thick] plot [smooth,domain=3:4] 
		(\x,{2+(\x-3)*(\x-5)});
		\draw [red,thick] plot [smooth,domain=4:5] 
		(\x,{3+2*(\x-3)*(\x-5)});
		\draw (1,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (2,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (3,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (4,1) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (5,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\end{tikzpicture}
	\end{center}
	Les solutions de l'inéquation $f(x) < 2$ sont les points de l'intervalle $\intv[o]{3}{4.71}$.
\end{exemple}

\begin{proposition}
	Les solutions de l'équation $f(x) < g(x)$ sont les abscisses des points où la courbe $\mathscr{C}_f$ est sous la courbe $\mathscr{C}_g$.
\end{proposition}

\begin{exemple}
	~\\[7pt]
	\begin{center}
		\begin{tikzpicture}[>=latex]
		\draw [blue!25] (-1,-1) grid (7,5) ;
		\draw [thick,->] (-1,0) -- (7,0) ;
		\draw [thick,->] (0,-1) -- (0,5) ;
		\foreach \i in {1,2,5,6} { \draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; }
		\foreach \i in {1,2,3,4} { \draw (0.1,\i) -- (-0.1,\i) node [left] {$\i$} ; }
		
		\draw [red,thick] plot [smooth,domain=1:3] 
		(\x,{2-(\x-1)*(\x-3)});
		\draw [red,thick] plot [smooth,domain=3:4] 
		(\x,{2+(\x-3)*(\x-5)});
		\draw [red,thick] plot [smooth,domain=4:5.25] 
		(\x,{3+2*(\x-3)*(\x-5)});
		\draw (1,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (2,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (3,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (4,1) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (5,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		
		\draw [green!50!black,thick] plot [smooth,domain=1:6]
		(\x,{3-0.5*(\x-2)*(\x-5)});
		
		\draw [very thick,blue] (2,0) node {$]$} -- (5,0) node {$[$} ;
		
		\end{tikzpicture}
	\end{center}
	Les solutions de \tikz[anchor=base, baseline]{\node [red] {$f(x)$ } }$<$\tikz[anchor=base, baseline]{\node [green!50!black] {$g(x)$ } } sont $x \in \intv[o]{2}{5}$.
\end{exemple}

\section{Variations d'une fonction}

\subsection{Vocabulaire des variations}

\begin{definition}
	Soient $f$ une fonction et $I$ un intervalle inclus dans l'intervalle de définition de $f$.
	\begin{itemize}
		\item On dit que $f$ est croissante sur $I$ lorsque pour tout $a$ et $b$ éléments de $I$ tels que $a < b$ alors $f(a) < f(b)$.
		\item On dit que $f$ est décroissante sur $I$ lorsque pour tout $a$ et $b$ éléments de $I$ tels que $a < b$ alors $f(a) > f(b)$.
	\end{itemize}
\end{definition}

\begin{savoirfaire}[Lire les variations sur un graphique]
	\begin{center}
		\begin{tikzpicture}[>=latex]
		\draw [blue!25] (-1,-1) grid (6,4) ;
		\draw [thick,->] (-1,0) -- (6,0) ;
		\draw [thick,->] (0,-1) -- (0,4) ;
		\foreach \i in {1,2,3,4,5} { \draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; }
		\foreach \i in {1,2,3} { \draw (0.1,\i) -- (-0.1,\i) node [left] {$\i$} ; }
			
		\draw [red,thick] plot [smooth,domain=1:3] 
		(\x,{2-(\x-1)*(\x-3)});
		\draw [red,thick] plot [smooth,domain=3:4] 
		(\x,{2+(\x-3)*(\x-5)});
		\draw [red,thick] plot [smooth,domain=4:5] 
		(\x,{3+2*(\x-3)*(\x-5)});
		\draw (1,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (2,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (3,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (4,1) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (5,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\end{tikzpicture}
	\end{center}
	Sur le graphique, la fonction «monte» entre $x = 1$ et $x = 2$. On dit qu'elle est croissante. Elle «descend» sur $I = \intv{2}{4}$. On dit qu'elle est décroissante sur $I$. Enfin, elle est croissante sur $\intv{4}{5}$.
\end{savoirfaire}

\begin{proposition}
	Soit $f$ une fonction définie sur $I$ un intervalle.
	\begin{itemize}
		\item Si $f$ est croissante, alors deux nombres quelconques de $I$ auront leurs images rangées dans le même ordre. On dit que les fonctions croissantes conservent l'ordre.
		\item Si $f$ est croissante, alors deux nombres quelconques de $I$ auront leurs images rangées dans l'ordre inverse. On dit que les fonctions décroissantes changent l'ordre.
	\end{itemize}
\end{proposition}

\begin{savoirrediger}[Savoir rédiger qu'une fonction est croissante]
	Soit $f$ une fonction dont on connaît une expression. Pour rédiger qu'elle est par exemple croissante, on peut 
	\begin{itemize}
		\item dire que c'est la somme ou le produit de fonctions croissantes ;
		\item prendre $a < b$ et appliquer l'expression à droite et à gauche du signe $<$ jusqu'à obtenir le résultat, par équivalence successives.
	\end{itemize}
\end{savoirrediger}

\subsection{Tableau de variations}

\begin{definition}
	Un tableau de variation contient les informations sur les variations d'une fonction donnée. On note par une flèche vers le haut les intervalles où la fonction est croissante et une flèche vers le bas les intervalles où la fonction est décroissante.
\end{definition}

\begin{exemple}[Tableau de variation]
	\leavevmode
		\begin{center}
		\begin{tikzpicture}[>=latex]
		\draw [blue!25] (-1,-1) grid (6,4) ;
		\draw [thick,->] (-1,0) -- (6,0) ;
		\draw [thick,->] (0,-1) -- (0,4) ;
		\foreach \i in {1,2,3,4,5} { \draw (\i,0.1) -- (\i,-0.1) node [below] {$\i$} ; }
		\foreach \i in {1,2,3} { \draw (0.1,\i) -- (-0.1,\i) node [left] {$\i$} ; }
		
		\draw [red,thick] plot [smooth,domain=1:3] 
		(\x,{2-(\x-1)*(\x-3)});
		\draw [red,thick] plot [smooth,domain=3:4] 
		(\x,{2+(\x-3)*(\x-5)});
		\draw [red,thick] plot [smooth,domain=4:5] 
		(\x,{3+2*(\x-3)*(\x-5)});
		\draw (1,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (2,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (3,2) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (4,1) node [cross=3pt,rotate=45,very thick,red] {} ;
		\draw (5,3) node [cross=3pt,rotate=45,very thick,red] {} ;
		\end{tikzpicture}
		
		a pour tableau de variation.
		
		\begin{tikzpicture}[scale=0.7]
			\tkzTabInit[deltacl=1]{$x$/1,$f$/3}{$1$,$2$,$4$,$5$}
			\tkzTabVar{-/$2$,+/$3$,-/$1$,+/$3$}
		\end{tikzpicture}
	\end{center}
\end{exemple}

\begin{savoirfaire}[Tracer une courbe compatible avec un tableau de variation]
	%TODO
\end{savoirfaire}

\subsection{Extremum}

\begin{definition}
	On appelle extremum aussi bien maximum que minimum, sans aucune distinction. Soit $f$ une fonction définie sur $D$ et $I$ un intervalle inclus dans $D$.
	\begin{itemize}
		\item Dire que $f$ admet un maximum sur $I$ signifie qu'il existe une valeur $a$ telle que pour tout $x$ de $I$, \[ f(x) ≤ f(a). \]
		\item Dire que $f$ admet un minimum sur $I$ signifie qu'il existe une valeur $a$ telle que pour tout $x$ de $I$, \[ f(x) ≥ f(a). \]
	\end{itemize}
\end{definition}

\begin{savoirfaire}[Identifier un maximum ou un minimum par lecture graphique]
	Dans l'exemple précédent, sur $I = \intv{1}{4}$, la courbe «ne va pas plus haut» que 3, qui est atteint en $x = 2$. Le maximum est donc $2$ et vaut $f(2) = 3$.\\
	De même, sur tout l'intervalle de définition, le minimum est $1$, pour $x = 4$.
\end{savoirfaire}

\begin{remarque}
	Le tableau de variations contient les extremums au bout des différentes flèches.
\end{remarque}

\begin{savoirrediger}[Rédiger la vérification qu'une certaine valeur est un maximum (ou un minimum)]
	Soit $f$ une fonction dont on connaît une expression. On souhaite savoir si $m$, donné, est un minimum de la fonction sur $I$. Pour cela, il faut savoir si $f(x) < m$ pour tout $x \in I$, ce qui est équivalent à $f(x) - m < 0$, ce qui est souvent plus facile à montrer.
\end{savoirrediger}
