\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\frenchbsetup{ItemLabels=\textbullet,}

\usepackage{tabularx}
\usepackage{enumitem}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.2cm,bottom=1.4cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{0.98\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\newcommand{\N}{\mathbf{N}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\R}{\mathbf{R}}

\title{DM \no 4}
\author{2\up{nde}}
\date{mai 2017}

\begin{document}

\maketitle

Le but de ce devoir est de démontrer un résultat sur le signe d'un
polynôme du second degré, ainsi que résoudre dans certains cas
l'équation $x^2 + 2 bx + c = 0$

\section{Recherche d'une règle pour le signe.}

À la calculatrice, affiche les fonctions dont les expressions sont :
\begin{multicols}{3}
  \begin{itemize}
    \item $f(x) = x^2 - 6x +  5$
    \item $g(x) = -x^2 + 6x -  5$
    \item $h(x) = -(x^2 - 6x +  5)$
  \end{itemize}
\end{multicols}

\begin{enumerate}
  \item Que peut-on dire de $f$ et $h$ ? de $h$ et $g$ ?
  \item Donner les solutions de $x^2 - 6x +  5 = 0$.
  \item Dresser le tableau de signe de $f$, $g$, et $h$.
    \begin{definition}[racines d'un polynômes]
      On appelle racine d'un polynôme, tout nombre qui annule un
      polynôme.
    \end{definition}

  \item Complèter les propositions :
    \begin{itemize}
      \item Si le coefficient de $x^2$ est positif, le polynôme est
        \dots\dots\dots\dots\dots\dots à l'intérieur des racines et
        \dots\dots\dots\dots\dots\dots à l'extérieur des racines.

      \item Si le coefficient de $x^2$ est négatif, le polynôme est
        \dots\dots\dots\dots\dots\dots à l'intérieur des racines et
        \dots\dots\dots\dots\dots\dots à l'extérieur des racines.
    \end{itemize}
\end{enumerate}

\section{Forme canonique}

\begin{definition}[fonction polynôme]
    On appelle fonction polynôme du second degré une fonction dont
    l'expression peut se mettre sous la forme $ax^2+bx+c$, avec $a\neq
    0$, $a,b,c\in \R$.
\end{definition}

\begin{enumerate}
    \item Donner trois exemples de polynômes du second degré.
\end{enumerate}

Désormais, pour des raisons de simplicité, nous ne considérerons que des
polynômes unitaires, c'est à dire dont le coefficient de $x^2$ vaut 1.

\begin{definition}[forme factorisée]
    Sous réserve d'existence, on appelle forme factorisée d'un polynôme
    la forme $(x-x_1)(x-x_2)$.
\end{definition}

\begin{enumerate}[resume]
    \item Donner un exemple de polynôme sous sa forme factorisée et sous
      sa forme développée.
    \item Entourer le sens qui semble le plus facile :
    \begin{itemize}
        \item forme factorisée $\rightarrow$ forme développée
        \item forme développée $\rightarrow$ forme factorisée
    \end{itemize}
\end{enumerate}

Soit $f:x\mapsto x^2 - 10x + 24$.

\begin{enumerate}[resume]
    \item Peut-on écrire $x^2 - 10x$ comme le début d'une identité
      remarquable ?
    \item Recopier et complèter :
      \begin{itemize}
        \item $(x + \dots )^2 = x^2 + \dots x + \dots$
        \item $x^2 - 10x = (x + \dots)^2 - \dots$
        \item $x^2 - 10x + 24 = (x + \dots)^2 - \dots + \dots = (x +
          \dots)^2 - \dots$
      \end{itemize}
    \item Peut-on factoriser cette forme ? La factoriser.
    \item Quelle est le type d'équation sous forme factorisée ?
    \item Après avoir résolu cette équation, vérifie que les nombres
      trouvés sont bien solutions de $f(x) = 0$.
    \item Résoudre, avec cette méthode, les équations suivantes :
      \begin{multicols}{3}
        \begin{itemize}
          \item $x^2 - 6x +  5 = 0$
          \item $x^2 - 6x +  8 = 0$
          \item $x^2 - 6x +  9 = 0$
          \item $x^2 - 6x + 11 = 0$
        \end{itemize}
      \end{multicols}

    \item Pourquoi peut-on en résoudre certaines et pas d'autres ?

      Proposer une interprétation géométique à partir de la forme
      canonique.
  \end{enumerate}

\section{Une condition pour résoudre les équations}

On va chercher à résoudre des équations dont la forme est $x^2 + 2bx +
c = 0$, en donnant une condition sur les valeurs $b$ et $c$.

\begin{enumerate}[resume]
  \item Identifier pour les 5 expressions précédentes que vous avez
    résolu les valeurs de $b$ et $c$.
  \item Identifier $x^2 + 2bx$ comme le début d'un carré. Ajuster
    ensuite pour obtenir une différence de deux carrés qui soit
    équivalente à l'équation de départ.
  \item Peut-on donner une condition pour que l'équation possède des
    solutions.
  \item Donner l'expression des solutions dans ce cas.
  \item Comment pourrait-on procéder si le coefficient en $x$ n'est pas
    un double ?
  \item Comment pourrait-on procéder si le coefficient en $x^2$ n'était
    pas 1 ?
\end{enumerate}

\end{document}
