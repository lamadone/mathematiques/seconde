\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\Vecteur}{\overrightarrow}

\makeatother


\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}

\newtheorem{preuve}{Preuve}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}


\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Vecteurs}
\author{2\up{nde}}
\date{}

\begin{document}

\maketitle
\textcolor{teal}{\section{Le vecteur, expression d'un déplacement}}

\begin{definition}
  Soit $A$ et $B$ un couple de point. On  admet qu'il existe une unique translation  qui  transforme le point $A$ en un point $B$ donné.\\
  Cette unique translation définit ainsi, le vecteur $\Vecteur{AB}$.
\end{definition}


\begin{definition}
  On définit, pour un vecteur $\Vecteur{AB}$ donné, les trois
  informations suivantes :
  \begin{itemize}
    \item \textcolor{blue}{ direction} -- c'est la droite $(AB)$ qui porte le vecteur ;
    \item \textcolor{blue}{  sens} -- pour le vecteur précédent, de $A$ vers $B$ ;
    \item \textcolor{blue}{ norme} -- c'est la « longueur » du vecteur.
  \end{itemize}
  On note cette dernière quantité $\lVert\Vecteur{AB}\rVert$.
\end{definition}

\begin{propriete}[égalité de deux vecteurs]
  Deux vecteurs sont égaux si et seulement si ils définissent le même
  déplacement
\end{propriete}
Une conséquence immédiate  :
\begin{propriete}
  Deux vecteurs sont égaux si et seulement si ils ont :
  \begin{itemize}
    \item même direction ;
    \item même sens ;
    \item même norme.
  \end{itemize}
\end{propriete}

Interprétation géométrique de l'égalité de deux vecteurs :
\begin{propriete}
  Deux vecteurs $\Vecteur{AB}$ et $\Vecteur{CD}$ sont \textcolor{blue}{ égaux } si et
  seulement si $ABDC$ est un \textcolor{blue}{ parallélogramme}.
\end{propriete}

\begin{center}
  \begin{tikzpicture}[yscale=0.5]
    \draw[->] (0,0) node[below left] {$A$} -- (7,4) node[right] {$B$} ;
    \draw[->,xshift=-50,yshift=30] (0,0) node[left] {$C$} -- (7,4)
    node[above right] {$D$} ;
    \draw[dashed] (0,0) -- (0-50pt,0+30pt) ;
    \draw[dashed] (7,4) -- (7cm-50pt,4cm+30pt) ;
  \end{tikzpicture}
\end{center}

\begin{definition}[notation du vecteur opposé]
  Le vecteur opposé au vecteur $\Vecteur{AB}$ est noté $\Vecteur{BA} = -\Vecteur{AB}$
\end{definition}

\begin{propriete}[caractérisation du milieu d'un segment]
  $I$ est le milieu du segment $AB$ si et seulement si $\Vecteur{AI} =
  \Vecteur{IB}$
\end{propriete}

\begin{center}
  \begin{tikzpicture}[yscale=0.7]
    \draw[-stealth] (0,0) node[below left] {$A$} -- (3.5,2) node[below right] {$I$};
    \draw[->]    (3.5,2) --(7,4) node[right] {$B$} ;
  \end{tikzpicture}
\end{center}
\textcolor{teal}{\section{Opérations sur les vecteurs}}

\begin{definition}[vecteur nul]
  On admet l'existence d'un vecteur \textcolor{blue}{nul}, correspondant à la translation
  qui déplace le point $A$ sur lui-même.

  On note $\Vecteur{AA} = \vec{0}$.
\end{definition}
\textcolor{violet}{\subsection{Somme de vecteurs}}

\begin{definition}[somme de vecteurs]
  Soit $\vec{u}$ et $\vec{v}$ deux vecteurs du plan. On définit la \textcolor{blue}{somme}
  de deux vecteurs comme le déplacement équivalent à ces deux
  déplacements successifs.

  Géométriquement, la construction est comme il suit :
  \begin{center}
    \begin{tikzpicture}
      \draw [->] (0,0) -- (2,1) ;
      \draw [->,shift={(-0.5,-1)}] (0,0) -- (3,-2) ;
      \draw [->,dashed] (0,0) -- (3,-2) ;
      \draw [->,dashed,shift={(2,1)}] (0,0) -- (3,-2) ;
      \draw [->,thick] (0,0) -- (2+3,1+-2) ;
      \draw (1,0.5) node [above] {$\vec{u}$} ;
      \draw [shift={(-0.5,-1)}] (1.5,-1) node [above] {$\vec{v}$} ;
      \draw ({(2+3)/2},{(1+-2)/2}) node [above] {$\vec{u}+\vec{v}$} ;
    \end{tikzpicture}
  \end{center}
\end{definition}

Le protocole de construction est le suivant :
\begin{itemize}
  \item tracer la droite parallèle à la droite portant $\vec{v}$ passant
    par l'extrémité du vecteur $\vec{u}$ ;
  \item reporter la longueur (norme) du vecteur $\vec{v}$ sur cette
    droite ;
  \item le point ainsi obtenu est l'extrémité d'arrivée du vecteur somme
    $\vec{u}+\vec{v}$ ;
  \item le vecteur somme est le vecteur qui part du point de départ de
    $\vec{u}$ et qui va à l'extrémité obtenue à l'étape précédente.
\end{itemize}

$\vec{u} + \vec{v} = \vec{v} +
\vec{v}$.

$\vec{u} + (\vec{v} +
\vec{w} ) = (\vec{u} + \vec{v} ) + \vec{w}$.

\begin{propriete}[relation de Chasles]
  Pour tout vecteur $\Vecteur{AB}$ et pour tout point $C$, on a
  l'égalité suivante : \[ \Vecteur{AB} = \Vecteur{AC} + \Vecteur{CB} \]
\end{propriete}

\begin{center}
  \begin{tikzpicture}
    \draw (0,0) node [below] {$A$} ;
    \draw (2,3) node [right] {$B$} ;
    \draw (4,2) node [above right] {$C$} ;
    \draw (-3,0) node [left] {$C'$} ;

    \draw [->] (0,0) -- (2,3) ;
    \draw [->] (0,0) -- (4,2) ;
    \draw [->] (4,2) -- (2,3) ;

    \draw [->] (0,0) -- (-3,0) ;
    \draw [->] (-3,0) -- (2,3) ;
  \end{tikzpicture}
\end{center}

\textcolor{violet}{\subsection{Différence de vecteurs}}


Ainsi, $\vec{u} - \vec{v} = \vec{u} + (- \vec{v}) $.

\begin{center}
  \begin{tikzpicture}
    \draw [->] (3,-2) -- (5,-1) ;
    \draw [->,shift={(-0.5,-1)}] (0,0) -- (3,-2) ;
    \draw [->,dashed] (0,0) -- (3,-2) ;
    \draw [->,dashed,shift={(2,1)}] (0,0) -- (3,-2) ;
    \draw [->,thick,shift={(3,-2)}] (0,0) -- (2-3,1--2) ;
    \draw (4,-1.5) node [above] {$\vec{u}$} ;
    \draw [shift={(-0.5,-1)}] (1.5,-1) node [above] {$\vec{v}$} ;
    \draw ({(2+3)/2},{(1+-2)/2}) node [above] {$\vec{u}-\vec{v}$} ;
  \end{tikzpicture}
\end{center}

\textcolor{violet}{\subsection{Produit d'un vecteur par un nombre réel}}

Commençons par le produit d'un vecteur par un nombre entier positif : il
s'agit simplement, de reporter, dans la même direction et dans le même
sens la longueur (ou norme du vecteur).
\begin{center}
  \begin{tikzpicture}
    \draw [thick,->, gray] (0,0) -- (2,1) ;
    \draw [->,shift={(2,1)},red, very thick] (0,0) -- (2,1) ;
  \end{tikzpicture}
\end{center}

On peut, assez facilement, définir le produit d'un vecteur par un entier
négatif : on reporte le vecteur autant de fois que nécessaire, mais dans
la direction opposée.
\begin{center}
  \begin{tikzpicture}
    \draw [thick,->, gray] (0,0) -- (2,1) ;
    \draw [->,shift={(0,0)}, red] (0,0) -- (-2*2,-1*2) ;
  \end{tikzpicture}
\end{center}


\begin{definition}[produit d'un vecteur par un réel]

  Soit $\vec{u}$ un vecteur du plan et $k$ un nombre réel.
  
  \begin{itemize}
    \item si $k>0$, $k\vec{u}$ est un vecteur de même direction et même
      sens que $\vec{u}$, et de norme $k\lVert\vec{u}\rVert$ ;
    \item si $k<0$, $k\vec{u}$ est un vecteur de même direction que
      $\vec{u}$, de sens opposé à  $\vec{u}$, et de norme
      $-k\lVert\vec{u}\rVert$ ;
    \item si $k=0$, $k\vec{u} = \vec{0}$
  \end{itemize}
\end{definition}

\begin{propriete}[distributivité des scalaires et des vecteurs]

  \begin{itemize}
    \item $k,k'$ des nombres réels et $\vec{u}$ un vecteur,
      $(k+k')\vec{u} = k\vec{u} + k'\vec{v}$
      
    \item $k$ est un réel  et $\vec{u}$ et $\vec{v}$ deux
      vecteurs, $k(\vec{u} + \vec{v}) = k\vec{u} + k\vec{v}$.
  \end{itemize}
\end{propriete}


\end{document}
