\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage{tkz-tab}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{break}
\newtheorem{exemple}{Exemple}
\newtheorem{proposition}{Proposition}
\newtheorem{propriete}{Propriété}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\newcommand{\R}{\mathbf{R}}

\title{Équations \& inéquations}
\author{J.B.S. 2\up{nde}}
\date{}

\begin{document}

\maketitle

\hfill\textit{Les «inconnus» gagnent à être connus.}\hfill\hfill

\section{Équations}
\subsection{Les plus simples}

Commençons par les plus simples d'entre elles : les \emph{équations} du
\emph{premier degré} à une inconnue. Il s'agit des égalités comportant
un et un seul élement inconnu, celui-ci n'étant pas multiplié par autre
chose que des quantités connues. Sous forme un peu plus mathématique,
une équation est une égalité de la forme $ax+b = cx+d$, vérifiée en tant
qu'égalité, où $x$ est un nombre réel inconnu et $a$, $b$, $c$ et $d$
quatre nombres réels connus, avec $a-c \neq 0$.

Une telle équation peut (et doit) être résolue à l'aide des
manipulations algébriques élémentaires qui ont été vues à propos des
ensembles de nombres. Ainsi, on peut additionner ou soustraire une même
quantité à droite \emph{et} à gauche du signe «$=$» ou encore multiplier
ou diviser par une même quantité non nulle à droite \emph{et} à gauche
du signe «=». Ainsi la solution générale de l'équation précédente est \[
x = \frac{d - b}{a - c}. \] Cette solution s'obtient de la façon
suivante :
\begin{align*}
  ax + b = & cx + d \\
  ax = & cx + d - b \\
  ax - cx = & d - b \\
  (a - c)x = & d - b \\
  x = & \frac{d - b}{a - c}
\end{align*}
Ces équations étant équivalentes d'une ligne à l'autre, à condition que
$a \neq c$, ou encore, si $c=0$, $a\neq 0$.

\subsection{Équations d'un ordre supérieur dans des cas «simples»}
Dans le cas de telles équations, la première chose à faire est de
«réarranger» les termes afin que ceux-ci soient tous dans le membre de
gauche et que le membre de droite soit égale à 0. Ainsi, de façon assez
générale, on pourra considérer des équations de la forme $a_0 + a_1x +
a_2x^2 + a_3x^3 + \cdots a_nx^n$. Le plus grand entier $i$ pour lequel
le coefficient $a_i$ est différent de 0 est le degré de cette équation.

La méthode principale de résolution de telles équations repose sur
l'obtention de la forme factorisée de l'équation. Néanmoins, la
factorisation étant une opération qui ne peut pas toujours être
effectuée, nous ne pourrons résoudre ces équations que dans les cas
suivants :
\begin{itemize}
  \item une indication est donnée quant à la factorisation de
    l'expression ;
  \item l'expression est une identité remarquable (surtout au degré 2).
\end{itemize}
Les indications données quant à la factorisation de l'expression peuvent
prendre diverses forme :
\begin{itemize}
  \item démonstration d'une égalité de factorisation ;
  \item vérification d'une solution particulière puis résolution d'un
    système particulier
\end{itemize}

\begin{exemple}
  On peut vérifier la factorisation suivante : \[ (x^2+3x+1)^2 - 1 =
  x(x+1)(x+2)(x+3), \] puis l'utiliser pour résoudre l'équation \[
  (x^2+3x+1)^2 = 1 \] de la façon suivante :
  \begin{align*}
    (x^2+3x+1)^2 = & 1 & \iff \\
    (x^2+3x+1)^2 - 1 = & 0 & \iff \\
    x(x+1)(x+2)(x+3) = & 0 & \iff \\
    x=0 \vee x+1 = 0 \vee x+2=0 \vee x+3 = 0 & & \iff \\
    x=0 \vee x = -1 \vee x=-2 \vee x = -3 & & \\
  \end{align*}
\end{exemple}

Pour les polynômes, on peut également, dans certains cas, trouver une
racine «évidente», puis procéder par identification comme dans l'exemple
suitvant :
\begin{exemple}
  Résoudre l'équation $x^2+5x-6 = 0$

  Posons $f:x\mapsto x^2+5x-6$. Un rapide calcul nous montre que
  $f(1)=0$. On dit alors que 1 est une racine «évidente» du polynôme.

  Soient $a$ et $b$ deux réels inconnus, tels que $x^2+5x-6 = (x - 1)(ax
  + b)$.

  Il est clair que ces deux expressionns s'annulent toutes deux en 1. Il
  faut désormais déterminer les réels $a$ et $b$. Pour cela, développons
  le membre de droite :
  $(x - 1)(ax + b) = ax^2 + bx -1\times ax - 1\times b = ax^2 + (b - a)x
  - b$.

  On peut désormais donner $a=1$ et $b=6$ et la factorisation associée :
  \[ x^2+5x-6 = (x-1)(x+6). \]

  Ainsi, les solutions de l'équation $x^2+5x-6 = 0$ sont $x_1 = 1$ et
  $x_2 = -6$, qu'on peut écrire $S=\{-6 ; 1 \}$
\end{exemple}

Cette étape de factorisation est essentielle, on le verra par la suite
lors de la détermination du signe d'une expression.

\section{Règles sur les inéquations}

Une partie de ces règles sont connues depuis Euclide, qui les formule
ainsi dans ses Éléments :
\begin{quote}
  Si à des choses inégales, on ajoute des quantités égales, alors les
  résultats restent inégaux
\end{quote}

Il faut comprendre cette citation de la façon suivante :
\[ a < b \implies a + c < b + c,\]
\[a > b \implies a + c > b + c. \]

Ces inégalités restent vraies lorsque les inégalités sont larges
($\leq$) au lieu d'être strictes ($<$). De même, il n'y aucune autre
hypothèse de faite sur la nature de $a$, $b$ et $c$ autre que le fait
que tous trois soient des nombres réels.

En revanche, il faut distinguer la multiplication par un nombre positif
et par un nombre négatif :
\[ k > 0 ; a < b \implies ka < kb \]
\[ k < 0 ; a < b \implies ka > kb \]

En pratique, on fait souvent des comparaisons à 0, surtout dans le cas
de la détermination du signe d'un polynôme. On présente souvent cette
information dans un tableau, dit tableau de signe.

\begin{exemple}
  Posons $f:x\mapsto  (x^2+3x+1)^2 - 1$. On veut déterminer le signe de
  $f$ sur l'ensemble $\R$.
  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[espcl=1.5]
      {$x$ / 1 , $x$ / 1  , $x+1$ / 1, $x+2$ / 1, $x+3$ / 1 ,$f(x)$ /1 }%
      {$-\infty$,$-3$ , $-2$ , $-1$, $0$, $+\infty$ }%
      \tkzTabLine{ , - , t, - , t , - ,t, - ,z, + }
      \tkzTabLine{ , - , t, - , t , - ,z, + ,t, + }
      \tkzTabLine{ , - , t, - , z , + ,t, + ,t, + }
      \tkzTabLine{ , - , z, + , t , + ,t, + ,t, + }
      \tkzTabLine{ , + , z, - , z , + ,z, - ,z, + }
    \end{tikzpicture}
  \end{center}
  On peut tracer la courbe représentative de cette fonction et vérifier
  les résultats fournis par le tableau de signe.
  \begin{center}
    \begin{tikzpicture}[]
      \draw [->] (-4.5,0) -- (1.5,0) ; \draw[->] (0,-1) -- (0,2) ;
      \draw plot [domain=-3.2:0.2,smooth] (\x,{\x*(\x*(\x*(\x+6)+11)+6) }) ;
    \end{tikzpicture}
  \end{center}
\end{exemple}
Ce tableau s'obtient, non pas en observant la courbe, mais par simple
application de la nature des inégalités sur les différents intervalles
et l'utilisation de la proposition suivante, vraie pour tous les
produits :
\begin{proposition}
  Le signe d'un produit est donné par la parité du nombre de facteurs
  négatifs :
  \begin{itemize}
    \item si le produit possède un nombre pair de facteurs négatifs, le
      résultat est positif ;
    \item sinon (le produit possède un nombre impair de facteurs
      négatifs) le résultat est négatif.
  \end{itemize}
\end{proposition}


\end{document}
