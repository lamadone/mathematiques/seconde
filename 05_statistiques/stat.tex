\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage{pstricks,pst-all,pstricks-add,pst-tree,pst-3dplot}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \hfill \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother


\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}

\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{teal}{}}




\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

% colonnes
\usepackage{multicol}
\setlength{\columnseprule}{0.55pt}
\setlength{\columnsep}{30pt}

\everymath{\displaystyle\everymath{}}

\title{Statistiques}
\author{2\up{nd}}
\date{}
\begin{document}
\maketitle\\




\section{Vocabulaire}
L’ensemble sur lequel porte l’étude statistique s’appelle la \textcolor{red}{\textit{population}}.\\
Un élément de cet ensemble est un \textcolor{red}{\textit{individu}}.\\
La population est étudiée selon un ou plusieurs \textcolor{red}{\textit{caractères}}.\\
Un caractère permet de déterminer une partition de la population selon ses diverses valeurs ( par exemple le sexe est un caractère à deux valeurs : masculin ou féminin).\\
Lorsque les valeurs d’un caractère sont des nombres, le caractère est dit \textcolor{red}{ \textit{quantitatif}}. \\
Lorsque les valeurs du caractère ne sont pas mesurables, le caractère est dit \textcolor{red}{\textit{qualitatif}}.\\
L’\textcolor{red}{\textit{effectif }}d’une classe statistique est le nombre d’éléments de la population observés dans cette classe.\\
La \textcolor{red}{\textit{fréquence}} d’une classe statistique est le rapport de l’effectif de cette classe à l’effectif total de la population. ( la fréquence peut être exprimée en pourcentage )

\begin{center}
fréquence = $\dfrac{\textbf{effectif}}{\textbf{effectif total}}$
\end{center}
\section{Caractéristiques}
\subsection{Moyenne arithmétique}

La \textcolor{red}{\emph{moyenne arithmétique}} d'une série statistique quantitative $S=\{x_1,x_2,\ldots,x_n\}$ est le nombre,
note $\overline{x}$ : \[\overline{x}=\frac{x_1+x_2+\ldots+x_n}{n}\]

\textbf{Remarque:\\}
\begin{itemize}
\item De la définition, on peut déduire que $n\overline{x}=x_1+x_2+\ldots+x_n$, ce qui peut s'interpréter de la manière suivante : \og La somme de toutes les valeurs de la série est inchangée si on remplace chaque valeur par $\overline{x}$ \fg. 
\item Si la série $S$ comporte $n$ données $x_1, x_2, \ldots, x_p$ d'effectifs respectifs (ou de fréquences respectives) $n_1$, $n_2$, \ldots, $n_p$,
alors $\overline{x}=\frac{n_1x_1+n_2x_2+\ldots+n_px_p}{\underbrace{n_1+n_2+\ldots+n_p}_{\text{effectif total}}}$
\end{itemize}


\subsection{M\'ediane}

On appelle \emph{médiane} d'une série statistique quantitative tout nombre $m$ tel que :
\begin{itemize}
	\item la moitié au moins des valeurs de la série est inférieure à $m$
	\item la moitié au moins des valeurs de la série est supérieure à $m$\\
\end{itemize}

\textbf{Remarque : \\}
\begin{itemize}
	\item Rappel : mathématiquement \og inférieur \fg{} et \og supérieur \fg{} signifient, en français, \og inférieur ou égal \fg{} et \og supérieur ou égal \fg.
	\item On admettra qu'un tel nombre existe toujours.
	\item La médiane partage la série en deux sous-séries ayant \emph{quasiment} le même effectif ; \emph{quasiment} car si plusieurs valeurs de la série sont égales à la médiane, les données inférieures à la médiane et les données supérieures à la médiane ne seront pas forcément en nombre égal.
	\item Il faut comprendre la médiane comme \og la valeur du milieu \fg.\\
\end{itemize}

Soit une série statistique quantitative comportant $n$ données : $S=\{x_1,x_2,\ldots,x_i,\ldots,x_n\}$ telles que $x_1\leqslant x_2\leqslant \ldots \leqslant x_n$.
\begin{itemize}
	\item Si $n$ est impair, la $\frac{n+1}{2}^{\text{ième}}$ donnée de la série est la médiane.
	\item Si $n$ est pair, tout nombre compris entre le $\frac{n}{2}^{\text{ième}}$ élément de la série et le suivant est \textbf{une} médiane ; dans le cadre scolaire \textbf{la} médiane sera la moyenne des deux données centrales de la série :
	      \[m=\frac{\left(\frac{n}{2}\right)^{\text{ième}}+\left(\frac{n}{2}+1\right)^{\text{ième}}}{2}\]
\end{itemize}

Exemple 1 : \\
\begin{center}
\begin{tabular}{|*{5}{c|}}
\hline
Couleurs & Blonds& Bruns & Châtains & Roux \\ \hline
effectifs & 25 &57 &91 &23 \\ \hline
\end{tabular}
Paramètres : $\overline{x}=1,85 ; med=2 ; Q_1=1 ; Q_3=3 $

\end{center}


Exemple 2 : \\
\begin{center}
\begin{tabular}{|*{4}{c|}}
\hline
Salaires & $[1\,000 \,;\, 2\,000[$ & $[2\,000 \,;\, 3\,000[$ & $[3\,000 \,;\, 4\,000[$ \\ \hline
Ouvriers & 114 & 66 & 20 \\ \hline
Centre de classe & 1 100 & 2 500& 3 500\\ \hline
\end{tabular}
\end{center}
Paramètres : $\overline{x}=1802 ; med=1100 ; Q_1=1100 ; Q_3=2500 $

Exemple 3 : 
\begin{center}
\begin{tabular}{|*{22}{c|}}\hline
Valeurs $x_i$ & 0 & 1 & 2& 3 & 4& 5\\ \hline
Effectifs $n_i$ & 3 & 5 & 6& 5 & 0& 1\\ \hline
\end{tabular}
\end{center}

Paramètres : $\overline{x}=1,85 ; med=2 ; Q_1=1 ; Q_3=3 $
\section{Mesures de dispersion}

 Elles visent à indiquer comment les données de la série statistique sont dispersées par rapport aux mesures centrales.

\subsection{Valeurs extr\^emes}

Les valeurs extrêmes d'une série quantitative sont ses valeurs \emph{minimale} et \emph{maximale} et l'\emph{étendue} est la différence entre les valeurs extrêmes de la série.

\subsection{Quartiles}

Quartiles dans le cas g\'en\'eral
Soit $S$ une série statistique quantitative.

\begin{itemize}
	\item On appelle \emph{premier quartile}, noté $Q_1$, tout réel tel que
			\begin{itemize}
				\item au moins 25\% des valeurs de la série ont une valeur inférieure ou égale à $Q_1$
			\item
			au moins 75\% des valeurs de la série ont une valeur supérieure ou égale à $Q_1$
			\end{itemize}
	\item On appelle \emph{deuxième quartile}, noté $Q_2$, tout réel tel que
			\begin{itemize}
				\item au moins 50\% des valeurs de la série ont une valeur inférieure ou égale à $Q_2$
			\item
			au moins 50\% des valeurs de la série ont une valeur supérieure ou égale à $m$
			\end{itemize}
		\item On appelle \emph{troisième quartile}, noté $Q_3$, tout réel tel que
			\begin{itemize}
				\item au moins 75\% des valeurs de la série ont une valeur inférieure ou égale à $Q_3$
			\item
			au moins 25\% des valeurs de la série ont une valeur supérieure ou égale à $Q_3$
			\end{itemize}
\end{itemize}

\begin{itemize}
 \item $Q_2$ est, par d\'efinition, la m\'ediane de la s\'erie.
 \item On admettra que de tels nombres existent toujours.
 \item La m\'ediane partage une s\'erie en deux sous-s\'eries ayant quasiment le m\^eme effectif (environ 50\,\%) ; les premier, troisi\`eme quartiles et la m\'ediane partageront une s\'erie en quatre sous-s\'eries ayant quasiment le m\^eme effectif (environ 25\,\%). \\
\end{itemize}

\textbf{Proposition\\}
Soit une série statistique quantitative comportant $n$ données : $S=\{x_1,x_2,\ldots,x_i,\ldots,x_n\}$ telles que $x_1\leqslant x_2\leqslant\ldots \leqslant x_n$. Alors :
\begin{itemize}
	\item La donn\'ee de rang $\frac{1}{4}n$ (arrondi au sup\'erieur si ce n'est pas un entier) convient toujours comme premier quartile.
	\item La donn\'ee de rang $\frac{3}{4}n$ (arrondi au sup\'erieur si ce n'est pas un entier) convient toujours comme troisi\`eme quartile.\\
	\end{itemize}

\textbf{Exemple\\}
\begin{itemize}
 \item S'il y a $n=29$ données dans la série, rangées dans l'ordre croissant :
\begin{itemize}
	\item $\frac{1}{4}\times29=7,25\approx 8$
	donc la huitième donnée de la série convient comme premier quartile ;
	\item $\frac{3}{4}\times29=21,75\approx 22$
	donc la vingt-deuxième donnée de la série convient comme troisième quartile.
	\end{itemize}
 \item S'il y a $n=64$ donn\'ees dans la s\'erie, rang\'ees dans l'ordre croissant : 
\begin{itemize}
	\item $\frac{1}{4}\times64=16$
	donc la seizi\`eme donnée de la série convient comme premier quartile ;
	\item $\frac{3}{4}\times64=48$
	donc la quarante huiti\`eme donnée de la série convient comme troisième quartile.\\
	\end{itemize}
\end{itemize}

\textbf{Proposition\\}
Soit une série statistique quantitative comportant $n$ données : $S=\{x_1,x_2,\ldots,x_i,\ldots,x_n\}$ avec $n\geqslant5$.

On ne change les déciles, les quartiles et la médiane si on remplace $x_1$ par n'importe quel nombre de l'intervalle $]-\infty\,;\,x_1]$ et $x_n$ par n'importe quel nombre de l'intervalle $[x_n\,;\,+\infty[$.


Comme on ne change pas le nombre de valeurs de la série, il y en aura toujours autant inférieures et supérieures à $D_1$, $Q_1$, $m$, $Q_3$ et $D_9$.


Une fois les premier et troisi\`eme quartiles disponibles, on d\'efinit l'\'ecart et l'intervalle interquartile de la mani\`ere suivante :

Définition\\
 Soit $S$ une s\'erie statistique quantitative et $Q_1$ et $Q_3$ ses premier et troisi\`eme quartiles. On appelle :
 \begin{itemize}
  \item \emph{écart interquartile} la différence $Q_3 - Q_1$ ;
  \item \emph{intervalle interquartile} l'intervalle $[Q_1 \,;\, Q_3]$.
 \end{itemize}



\section{Représentations graphiques}

Si les mesures centrales et les mesures de dispersion ont pour but de r\'esumer une s\'erie statistique en quelques nombres, les repr\'esentations graphiques, elles, visent \`a la visualiser.

\subsection{Diagramme à bâtons}

On considère la série :
%\vspace{-1em}
\begin{center}
\begin{tabular}{|*{22}{c|}}\hline
Valeurs $x_i$ & 0 & 1 & 2& 3 & 4& 5& 6\\ \hline
Effectifs $n_i$ & 3 & 5 & 6& 5 & 6& 7& 7\\ \hline
\end{tabular}
\end{center}

\begin{center}
\begin{tikzpicture}
  \tkzInit[xmin=0,xmax=6,ymin=0,ymax=7]
    \tkzAxeX[unit]
        \tkzAxeY[unit]
        \draw [very thick, blue] (0,0)--(0,3);        
        \draw [very thick, blue] (1,0)--(1,5);
        \draw [very thick, blue] (2,0)--(2,6);
        \draw [very thick, blue] (3,0)--(3,5);
        \draw [very thick, blue] (4,0)--(4,6);
        \draw [very thick, blue] (5,0)--(5,7);
        \draw [very thick, blue] (6,0)--(6,7);
\end{tikzpicture}
\end{center}


\subsection{Diagrammes bas\'es sur la fr\'equence}

 Les s\'eries statistiques peuvent aussi \^etre repr\'esent\'ees en diagrammes circulaires, semi-circulaires, rectangulaires, etc.
L'aire de chaque modalit\'e devra \^etre proportionnelle \`a l'effectif de cette modalit\'e.
Les fr\'equences permettent d'obtenir assez facilement la part du diagramme qui devra \^etre consacr\'ee \`a chaque modalit\'e.

 Ainsi si on consid\`ere la s\'erie suivante :
\begin{center}
\begin{tabular}{|*{5}{c|}}\hline
Donn\'ees & Blonds 	& Bruns & Ch\^atains & Roux  \\ \hline
Effectifs $n_i$ & 25		& 57		&91		&23 \\ \hline
\end{tabular}
\end{center}            

 On obtient les diagrammes de la figure\\
\begin{center}
 \begin{tikzpicture}
\draw[fill=red] (0,0)--(0:2.5)arc(0:52.2:2.5)--cycle;
\draw[fill=green] (0,0)--(52.2:2.5)arc(52.2:154.8:2.5)--cycle;
\draw[fill=blue] (0,0)--(154.8:2.5)arc(154.8:196.2:2.5)--cycle;
\draw[fill=violet] (0,0)--(196.2:2.5)arc(196.2:360:2.5)--cycle;
 \end{tikzpicture}
\end{center}



\subsection{Diagramme en boite}

 On peut représenter graphiquement les valeurs extrêmes, les quartiles et la médiane par un \emph{diagramme en boite}, appelé aussi \emph{boite à moustaches}, conçu de la manière suivante :
\begin{itemize}
	\item au centre une boite allant du premier au troisième quartile, séparée en deux par la médiane ;
	\item de chaque côté une moustache allant du minimum au premier quartile pour l'une, et du troisième quartile au maximum pour l'autre.
\end{itemize}

Ce type de diagramme permet une interprétation visuelle et rapide de la dispersion des séries statistiques. Il
permet également d'apprécier des différences entre des séries (lorsqu'elles ont des ordres de grandeurs
comparables).

\begin{tikzpicture}[scale=0.9]
\tkzInit[xmin=0,xmax=20]
\tkzAxeX
\begin{scope}[xshift= 5cm, yshift=0.5cm]
\draw rectangle(2,1) node[above] {Médiane};
\draw rectangle(10,1);
\end{scope}
\draw (2,1) node{$\bullet$} node[above]{min} -- (5,1) node {$\bullet$} node[right]{$Q_1$};
\draw (15,1)node {$\bullet$} node[left]{$Q_3$} -- (19,1)node{$\bullet$} node[above]{max};
\end{tikzpicture}

Remarque\\
\begin{itemize}
	\item La hauteur des boites est arbitraire (on les fait parfois proportionnelles à l'effectif total de la série).
	\item La boite contient les 50\% des données centrales.

\end{itemize}

\section{Exercices}


\begin{exercice}
On donne la série suivante : \begin{center}
11, 12,  13, 4, 17, 5, 13, 13, 5, 6, 6, 10, 10, 8, 9, 9, 11, 11, 14, 5, 14, 9, 9, 15, 7, 8, 15.                                                                                                                              \end{center}
\begin{enumerate}
	\item D\'eterminer la moyenne de la s\'erie.
	\item D\'eterminer la m\'ediane et les quartiles de la s\'erie.
	\item Représenter le diagramme en boîte correspondant.
	\item Quel est l'écart interquartile de la série ?
	\item Quel est l'intervalle interquartile de la série ?
	\end{enumerate}
\end{exercice}

\begin{exercice}
 Dans une classe, les notes sont les suivantes : \begin{center}
0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20.                                                                                                                          \end{center}
\begin{enumerate}
	\item D\'eterminer la moyenne de la s\'erie.
	\item D\'eterminer la m\'ediane et les quartiles de la s\'erie.
	\item Représenter le diagramme en boîte correspondant.
	\item Que remarque-t-on sur ce diagramme ? Pouvait-on s'y attendre ?
\end{enumerate}
\end{exercice}

\begin{exercice}

Le tableau ci-contre donne une répartition des salaires mensuels en euros des employés dans une entreprise.
\begin{enumerate}
	\item Quel est le nombre d'employés de l'entreprise ?
	\item Quel est le nombre d'employés touchant un salaire mensuel supérieur ou égal à 1\,200 \euro{} ?
	\item Estimer le salaire moyen et le salaire m\'edian des employés de l'entreprise.
\end{enumerate}
\begin{center}
\begin{tabular}{|*{6}{c|}}\hline
Salaire & $[1\,000\,;\,1\,200[$ & $[1\,200\,;\,1\,500[$ &$[1\,500\,;\,2\,000[$ & $[2\,000\,;\,3\,000[$ &$[3\,000\,;\,10\,000[$\\ \hline
 Effectif &326 &112 &35 & 8 & 3 \\ \hline
\end{tabular}
\end{center}
\end{exercice}

\begin{exercice}
Dans une petite ville fictive où la taxe d'habitation est proportionnelle à la superficie de
l'habitation, la répartition des habitations selon leur superficie est la suivante :
\begin{center}
\begin{tabular}{|*{7}{c|}}\hline
Superficie en m$^2$ & $[10\,;\,40[$ &$[40\,;\,70[$ &$[70\,;\,100[$ & $[100\,;\,120[$ &$[120\,;\,140[$ &$[140\,;\,170[$  \\ \hline
 Effectifs &14 &24 &54 &64&32&12\\ \hline
\end{tabular}
\end{center}
\begin{enumerate}
	\item Déterminer une valeur approchée de la superficie moyenne des
habitations de cette ville.
\item Un membre du conseil municipal propose d'exonérer la moitié des personnes : celles dont les
habitations ont les superficies les plus faibles. Une personne dont
l'appartement mesure 80 m$^2$ serait-elle exonérée ? Une personne dont
l'appartement mesure 110 m$^2$ serait-elle exonérée ?
\item Un autre membre du conseil municipal propose d'exonérer le quart des personnes : celles dont les habitations ont les superficies les
plus faibles. Une personne dont l'appartement mesure 80 m$^2$ serait-elle exonérée ?
\end{enumerate}
\end{exercice}


\begin{exercice}
Un entomologiste a fait des relevés sur la taille de 50 courtilières adultes dont voici les r\'esultats :
\begin{center}
33, 35, 36, 36, 37, 37, 37, 38, 38, 38, 39, 39, 39, 39, 40, 40, 40, 40, 40, 41, 41, 41, 41, 41, 41, 41, 42, 42, 42, 42, 42, 42, 43, 43, 43, 43, 44, 44, 44, 44, 45, 45, 45, 46, 46, 47, 47, 48, 48, 50.\end{center}
\begin{enumerate}
	\item Organiser les relevés dans le tableau d'effectifs suivant :\\
\footnotesize \begin{tabular}{|m{2.5cm}|*{18}{c|}}\hline
Valeur & 33&34&35&36&37&38&39&40&41&42&43&44&45&46&47&48&49&50\\ \hline
Effectif&&&&&&&&&&&&&&&&&&\\ \hline
Effectif cumulé croissant&&&&&&&&&&&&&&&&&&\\\hline
\end{tabular} \normalsize
\item Représenter les données par un diagramme à bâtons.
\item Calculer la moyenne de la série. Déterminer sa médiane.
Déterminer les premier et troisième quartiles.
\item Construire le diagramme en boîte correspondant.
\item Interpr\'eter les r\'esultats obtenus.
\end{enumerate}
\end{exercice}

\begin{exercice}
On a relevé le prix de vente d'un CD et le nombre de CD vendus chez différents fournisseurs. Les résultats forment une série statistique à une variable donnée par le tableau ci-dessous.
\begin{center}
\begin{tabular}{|*{6}{c|}}\hline
Prix de vente (en \euro) & 15& 16&17&18&19\\ \hline
Nombre de CD vendus & 83&48&32&20&17 \\ \hline
\end{tabular}
\end{center}
\begin{enumerate}
	\item Quelles sont les différentes valeurs de la série.
	\item Donner la fréquence correspondant à chacune de ces valeurs.
	\item Donner la moyenne et la m\'ediane de la série. Que représentent ces nombres ?
	\item Représenter la série par un diagramme semi-circulaire.
\end{enumerate}
\end{exercice}

\begin{exercice}
Dans une classe de 30 élèves, la moyenne des 20 filles est 11,5 et la moyenne des 10 garçons est 8,5. Donner la moyenne de classe en prouvant la validit\'e de votre calcul.
\end{exercice}

\begin{exercice}
Lors d'une étude d'une population de rats, K. Miescher a observé l'évolution d'une population de 144 rats. \\
Le tableau ci-contre indique la durée de vie (en mois) des rats.\\
Ainsi, un seul rat a vécu entre 10 et 15 mois, trois ont vécu entre 15 et 20 mois, neuf entre 20 et 25 mois etc.\\
On suppose que, dans chaque classe, la répartition est régulière.
\begin{enumerate}
	\item \'Evaluez l'étendue de cette série
	\item \'Evaluez la moyenne de la durée de vie d'un rat dans cette population
	\item Quelle est le rang de la durée de vie m\'ediane d'un rat dans cette population ?\\
	      \`A l'aide du polygone des effectifs cumul\'es croissants,\'evaluez le valeur de la
médiane ?
\item En observant la moyenne et la médiane, quel commentaire peut-on faire ?
\end{enumerate}
\begin{scriptsize}
\begin{tabular}{|*{13}{c|}}\hline
Durée de vie & $[10\,;\,15[$ &$[15\,;\,20[$ &$[20\,;\,25[$& $[25\,;\,28[$ & $[28\,;\,30[$ &$[30\,;\,32[$ &$[32\,;\,34[$ &$[34\,;\,36[$ &$[36\,;\,38[$ &$[38\,;\,40[$ &$[40\,;\,42[$ &$[42\,;\,43[$ \\ \hline
 Effectifs &1&3&9&12&13&20&23&26&22&11&3&1 \\ \hline
\end{tabular}
\end{scriptsize}
\end{exercice}

\begin{exercice}
Dans deux entreprises $A$ et $B$, les employés sont
classés en deux catégories : ouvriers et cadres.\\
Le tableau donne la
répartition des employés en fonction de leur
catégorie professionnelle et de leur salaire
mensuel net, en euros.

 \centering
Salaires des entreprise $A$ et $B$
 
 \begin{multicols}{2}
 \textbf{Entreprise $A$}

\vskip 0.5em

\begin{tabular}{|*{4}{c|}}
\hline
Salaires & 1\,500 & 2\,500 & 3\,500 \\ \hline
Ouvriers & 114 & 66 & 0 \\ \hline
Cadres & 0 & 8 & 12 \\ \hline
\end{tabular}


\textbf{Entreprise $B$}

\vskip 0.5em

\begin{tabular}{|*{4}{c|}}
\hline
Salaires & 1\,500 & 2\,500 & 3\,500 \\ \hline
Ouvriers & 84 & 42 & 0 \\ \hline
Cadres & 0 & 12 & 12 \\ \hline
\end{tabular}
\end{multicols}



\begin{enumerate}
	\item
\begin{enumerate}
	\item Calculer la moyenne des salaires de tous
les employés de l'entreprise $A$
\item Calculer la moyenne des salaires des
ouvriers de l'entreprise $A$
\item Calculer la moyenne des salaires des
cadres de l'entreprise $A$
\end{enumerate}
\item Faire les mêmes calculs pour l'entreprise $B$
\item Le P.D.G. de l'entreprise $A$ dit à celui de l'entreprise $B$ : \og Mes employés sont mieux payés que les vôtres. \fg

\og Faux \fg répond ce dernier, \og mes ouvriers sont mieux payés et mes cadres également. \fg\\
Expliquer ce paradoxe.
\end{enumerate}
\end{exercice}

\begin{exercice}
Le tableau suivant donne les effectifs des notes obtenues dans une classe en Mathématiques et en Histoire-Géographie :
\begin{center}
\begin{small}\begin{tabular}{|*{22}{c|}}
\hline
Notes & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7&  8 & 9 &10 &11 &12& 13 &14& 15 &16& 17& 18 &19 &20 \\ \hline
Maths& 0& 0 &0 &0 &1& 0& 1 &1 &3 &4& 4& 1& 3& 2&2 &1 &1& 0& 0& 0& 0\\ \hline
H.-G. &0 &1& 0& 0 &2 &0 &1 &2 &1& 1& 4& 2& 2& 0 &3& 2 &1 &0& 1& 0& 1\\ \hline
\end{tabular}\end{small}
\end{center}
 Le but de l'exercice est de comparer la dispersion des notes en Maths et en Histoire-Géographie.

\begin{enumerate}
	\item Calculer $\overline{x}$ et $\overline{x'}$ les moyennes respectives de Maths et d'Histoire-G\'eographie.
	\item Calculer médiane $m_e$ et quartiles $Q_1$ et $Q_3$ en Maths.
	\item Calculer médiane $m'_e$ et quartiles $Q'_1$ et $Q'_3$ en Histoire-Géographie.
	\item Représenter les diagrammes en boîte des notes en Maths et en Histoire-Géographie.
	\item Interpr\'eter les r\'esultats obtenus.
\end{enumerate}
\end{exercice}

\begin{exercice}
\end{exercice}
\end{document}