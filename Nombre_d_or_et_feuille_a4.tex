\documentclass[a4paper,french,12pt]{article}

\input{commons.tex.inc}

\author{}
\title{Sur le nombre d'or et le rapport des feuilles de papier DIN}
\date{avril 2019}

\begin{document}
\maketitle

\section{Définitions}

\subsection{Nombre d'or}

Le nombre d'or est connu depuis l'antiquité : on retrouve cette proportion
en observant le rapport de la facade du Parthénon d'Athènes par exemple. Il
défini une «classe» de rectangle considérés harmonieux de la façon suivante
:
\begin{definition}
  Si on retire un carré à un rectangle, alors on obtient un rectangle dont
  le rapport $\dfrac{L}{l}$, où $L$ est la longueur et $l$ la largeur est
  identique au rectangle initial.
\end{definition}

\begin{center}
  \begin{tikzpicture}[scale=0.6]
    \tkzDefPoint(0,0){A}
    \tkzDefPoint(8,0){B}
    \tkzDefGoldRectangle(A,B)
    \tkzGetPoints{C}{D}
    \tkzDefGoldRectangle(B,C)
    \tkzGetPoints{E}{F}
    \tkzDrawPolygon[color=red,fill=red!20](A,B,C,D)
    \tkzDrawPolygon[color=blue,fill=blue!20](B,C,E,F)
  \end{tikzpicture}
\end{center}

\subsection{Format A4}

L'idée principale des feuilles de papier A4 est la possibilité de retrouver
le même facteur de forme par juxtaposition des deux feuilles identiques.
Autrement dit, deux A4 collés par le plus grand côtés, sans recouvrement
donnent un A3 qui possède le même rapport $\dfrac{L}{l}$. Ceci permet de
faire aisément des photocopies en réduction ou agrandissement par exemple.

\begin{definition}
  On définit la série de papier A par :
  \begin{itemize}
    \item A0 : $\np[mm]{841} × \np[mm]{1189}$ soit une surface de
      $\np[m^2]{0.999949}$.
    \item A$(n+1)$ est A$(n)$ coupé en deux dans la plus grande dimension et
      en prenant comme nouvelle longueur l'ancienne largeur.
  \end{itemize}
\end{definition}

Le format A0 coïncide avec le format Grand-Aigle des plans cadastraux de
Napoléon Bonaparte et s'avère pratique pour peser le papier et vérifier le
grammage. Cependant, en remarquant qu'il faut 16 A4 pour produire un A0, on
peut aisément vérifier le grammage d'un papier.

\section{Recherche des équations menant aux nombres cherchés}

\subsection{Pour le nombre d'or}

Si on désigne par $x = \dfrac{L_i}{l_i}$ le rapport du grand côté au petit
côté du rectangle cherché, on obtient la proposition suivante :

\begin{proposition}
  Le rapport $x$ est solution de l'équation \[
  x - 1 = \frac{1}{x}
  .\] 
\end{proposition}

L'équation précédente étant écrite de façon non conventionnelle, on peut
écrire la proposition suivante.

\begin{proposition}
  L'équation précédente est équivalente à l'équation \[
  x^2 - x - 1 = 0
  .\] 
\end{proposition}
\begin{proof}
  \[
    x - 1 = \frac{1}{x} \iff x(x -1) = x \frac{1}{x} \iff x^2 - x = 1 \iff
    x^2 - x - 1 = 0
  .\] 
\end{proof}

\subsection{Pour les feuilles A4}

Si on désigne par $x = \dfrac{L_i}{l_i}$ le rapport du grand côté au petit
côté du rectangle cherché, on obtient la proposition suivante :

\begin{proposition}
  Le rapport $x$ du grand côté au petit côté est solution de l'équation \[
  \frac{1}{x} = \frac{x}{2}
  .\]
\end{proposition}

Cette équation n'étant pas nécessairement commode à résoudre, on peut
utiliser la proposition suivante.

\begin{proposition}
  \[
    \frac{1}{x} = \frac{x}{2} \iff x^2 - 2 = 0
  .\] 
\end{proposition}
\begin{proof}
  \[
    \frac{1}{x} = \frac{x}{2} \iff x \frac{1}{x} = x \frac{x}{2} \iff 1 =
    \frac{x^2}{2} \iff 2 = x^2 \iff x^2 -2 = 0
  .\] 
\end{proof}

\section{Solutions exactes}

\subsection{Nombre d'or}

\begin{proposition}
  $x^2 - x $ est le début du développement d'un carré.
\end{proposition}
\begin{proof}
  En effet, $\brk*{x - \frac{1}{2}}^2 = x^2 - 2×x×\frac{1}{2} +
  \frac{1}{4}$.
\end{proof}

\begin{proposition}
  $x^2 - x - 1 = \brk*{x - \frac{1}{2}}^2 - \frac{5}{4}$
\end{proposition}
\begin{proof}
  D'après la proposition précédente, on a $\brk*{x - \frac{1}{2}}^2 = x^2 -
  2×x×\frac{1}{2} + \frac{1}{4}$, d'où on tire que $\brk*{x - \frac{1}{2}}^2
  - \frac{5}{4} = x^2 - 2×x×\frac{1}{2} + \frac{1}{4} - \frac{5}{4} = x^2 -
  x - 1$.
\end{proof}

On peut donc résoudre cette dernière équation.

\begin{proposition}
  Les solutions de l'équation $x^2 - x - 1 = 0$ sont les solutions de
  l'équation $\brk*{x - \frac{1}{2}}^2 - \frac{5}{4} = 0$ qui se factorise
  en $\brk*{x - \frac{1}{2} + \frac{\sqrt{5}}{2}}\brk*{x - \frac{1}{2} -
  \frac{\sqrt{5}}{2}} = 0$
\end{proposition}
\begin{proof}
  Les deux expressions étant égales, elles ont les même solutions. Par
  ailleurs, on reconnaît une différence de carré : $a^2 - b^2$ qui se
  factorise en $a^2 - b^2 = (a-b)(a+b)$.
\end{proof}

\begin{proposition}
  La solution positive de l'équation est $\frac{\sqrt{5} + 1}{2}$.
\end{proposition}
\begin{proof}
  Il suffit de justifier que $1+\sqrt{5} > 0$ et $1 - \sqrt{5} < 0$.
\end{proof}

\subsection{Feuille A4}

\begin{proposition}
  L'équation $x^2 - 2 = 0$ admet pour solution positive $x = \sqrt{2}$.
\end{proposition}
\begin{proof}
  $x^2 - 2 = x^2 - \sqrt{2}^2 = (x - \sqrt{2})(x + \sqrt{2})$. Les solutions
  de $x^2 - 2 = 0$ sont
\end{proof}

\section{Solutions approchées}

\subsection{Fraction continue du nombre d'or}

On peut remarquer que l'équation $x - 1 = \dfrac{1}{x}$ peut aussi s'écrire
$x = 1 + \dfrac{1}{x}$. On peut donc, en remplaçant $x$, obtenir les
fractions \[
 \frac{1}{1+\frac{1}{1+\frac{1}{1+\frac{1}{…}}}}
.\] 

On obtient la suite de valeurs $1,2,\frac{3}{2},\frac{5}{3},\frac{8}{5},…$
qui sont des approximations du nombre d'or. Une autre façon de les calculer
est de remarquer que $3 = 2 + 1$, $5 = 3 + 2$, $8 = 5 + 3$ et de calculer le
rapport d'un terme au précédent. Cette suite de nombre est connue sous le
nom de suite de Fibonacci.

\subsection{Fraction continue de $\sqrt{2} $}

Partant de la suite précédente, on avait $a_{n+1} = a_n + a_{n-1}$.
Considérons la suite $a_{n+1} = 2a_n + a_{n-1}$. En partant des même valeurs
que précédement ($a_0 =1, a_1 = 1$), on obtient la suite $1,1,3,7,17,41$
dont le rapport d'un terme au précédent est de plus en plus proche de
$\sqrt{2}$. En s'inspirant du modèle précédent, on pose $x^2 = 2x + 1$ ou
encore $x^2 - 2x - 1 = 0$ qui possède comme solution positive $x = \sqrt{2}
+1$. On a donc \[
\sqrt{2}+1 = 2 + \frac{1}{2 + \frac{1}{2 + \frac{1}{…}}} 
\] ce qui permet d'écrire \[
\sqrt{2} = 1 + \frac{1}{2 + \frac{1}{2 + \frac{1}{…}}} 
\]

\section{Constructions géométriques associées}



\section*{Références}

\begin{itemize}
  \item \url{https://fr.wikipedia.org/wiki/Format_de_papier}
  \item \url{https://fr.wikipedia.org/wiki/ISO_216}
  \item \url{https://xavier.hubaut.info/coursmath/var/rectangl.htm}
  \item
    \url{https://lehollandaisvolant.net/?d=2011/02/27/16/15/55-dou-vient-la-dimension-a4}
  \item \url{https://www.cl.cam.ac.uk/~mgk25/iso-paper.html}
  \item \url{http://betweenborders.com/wordsmithing/a4-vs-us-letter/}
\end{itemize}

\end{document}
