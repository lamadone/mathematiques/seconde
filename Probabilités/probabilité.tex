\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}


\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{Lycée \textsc{LaSalle Saint-Denis}}}}
\rfoot{\footnotesize{Page \thepage/ \pageref{LastPage}}}
\rhead{}
\lhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}

\title{Probabilité}
\author{2\up{nd}}
\date{}

\begin{document}


\maketitle\\

\section{Vocabulaire des ensembles}


\begin{definition}[Intersection]
\begin{multicols}{2}
L'\textcolor{red}{intersection} de deux ensembles $A$ et $B$ est l'ensemble des \'el\'ements qui sont communs \`a $A$ et $B$.\\
On la note $A\cap B$.\\
\begin{tikzpicture}
\filldraw[fill=gray!20](1:30.5cm) ellipse (0.45cm and 0.2cm) ;
\draw [color=red,very thick] (1:30cm) ellipse (0.9cm and 0.3cm);
\draw [color=blue,very thick] (1:31cm) ellipse (0.9cm and 0.3cm) ;
\draw (29,0) node{$A$} (32,0) node{$B$};
\end{tikzpicture}
\end{multicols}
\end{definition}

\begin{remarque}
Lorsque $A\cap B= \varnothing$, on dit que les ensembles $A$ et $B$ sont disjoints.
\end{remarque}

\begin{definition}[R\'eunion]
\begin{multicols}{2}
La \textcolor{red}{r\'eunion} de deux ensembles $A$ et $B$ est l'ensemble des \'el\'ements qui sont dans $A$ ou dans $B$. \\
On la note $A \cup B$.
\begin{tikzpicture}
\filldraw[fill=gray!20](1:30cm) ellipse (0.9cm and 0.3cm)(1:31cm) (1:31cm) ellipse (0.9cm and 0.3cm);
\draw [color=red,very thick] (1:30cm) ellipse (0.9cm and 0.3cm);
\draw [color=blue,very thick] (1:31cm) ellipse (0.9cm and 0.3cm) ;
\draw (29,0) node{$A$} (32,0) node{$B$};
\end{tikzpicture}
\end{multicols}
\end{definition}

\begin{definition}[Inclusion]
\begin{multicols}{2}
On dit qu'un ensemble $A$ est \textcolor{red}{inclus} dans un ensemble $B$ si tous les \'el\'ements de $A$ sont des \'el\'ements
de $B$. \\
On note alors $A \subset B$ (\og $A$ inclus dans $B$ \fg) .
\begin{tikzpicture}
\draw [color=red,very thick] (1:30cm) ellipse (0.9cm and 0.3cm);
\draw [color=blue,very thick] (1:30.5cm) ellipse (0.3cm and 0.1cm) ;
\draw[blue] (30,0.5) node{$A$} ;
\end{tikzpicture}
\end{multicols}
\end{definition}

\begin{definition}[Compl\'ementaire]
\begin{multicols}{2}
Soit $E$ un ensemble et $A$ une partie de $E$. Le \textcolor{red}{compl\'ementaire} de $A$ dans $E$ est l'ensemble des \'el\'ements
de $E$ qui n'appartiennent pas \`a $A$. On le note $\overline{A}$.
\begin{tikzpicture}
\draw [fill=gray!20] (1:30cm) ellipse (1.5cm and 0.7cm);
\draw [color=red,very thick] (1:30cm) ellipse (1.5cm and 0.7cm) (29,0.5)node[right]{$\overline{A}$};
\draw [color=blue,very thick, fill=white] (1:30.5cm) ellipse (0.7cm and 0.3cm)node{$A$}  ;
\end{tikzpicture}
\end{multicols}
\end{definition}
\begin{remarque} $A \cup \overline{A} = E$ et $A \cap \overline{A} = \varnothing$. \end{remarque}
\section{Exp\'eriences al\'eatoires}

\subsection{Issues, univers}

\begin{definition}
Une exp\'erience est dite \textcolor{red}{al\'eatoire} lorsqu'elle a plusieurs issues (ou r\'esultats) possibles et que l'on ne peut ni pr\'evoir avec certitude, ni calculer laquelle de ces issues sera r\'ealis\'ee.\\L'ensemble de toutes les \textcolor{red}{issues} d'une \textcolor{red}{exp\'erience al\'eatoire} est appel\'e \textcolor{red}{univers} (ou univers des possibles). On note
g\'en\'eralement cet ensemble $\Omega$.
\end{definition}
\begin{exemple}
\begin{itemize}
	\item On lance un d\'e et on regarde la face obtenue : $\Omega = \{1\,;\,2\,;\,3\,;\,4\,;\,5\,;\,6\}$
	\item On lance un d\'e et on regarde si le num\'ero de la face obtenue est pair ou impair : $\Omega = \{P\,;\,I\}$
	\item On lance une pi\`ece de monnaie : $\Omega = \{P\,;\,F\}$
	\item On lance deux pi\`eces de monnaie, l'une apr\`es l'autre : $\Omega = \left\lbrace (P,F);(P,P);(F,F);(F,P)\right\rbrace $

\end{itemize}
\end{exemple}
Remarquons qu'une même exp\'erience peut d\'eboucher sur des univers diff\'erents suivant ce que l'on observe, comme le montrent les deux premiers exemples ci-dessus.
\subsection{\'Evénements}
\begin{definition}[Événement]
Un \textcolor{red}{événement} est un sous-ensemble de l'univers.\\
Il peut toujours se décrire à l'aide d'issues.  
\end{definition}

L'événement \textcolor{red}{certain} est composé de toutes les issues possibles. Il est noté $\Omega$\\
L'événement \textcolor{red}{impossible} ne contient aucune des issues possibles. Il est noté $\varnothing$\\
L'événement \textcolor{red}{contraire} de l'événement $A$ est composé de toutes les issues de l'univers qui ne sont pas dans $A$. Il est note $\overline{A}$.\\
\[A\cup \overline{A} = \Omega\]
\[A \cap \overline{A} = \varnothing\]
Des événements sont \textcolor{red}{incompatibles} s'ils n'ont aucun éléments commun.

\subsection{Loi de probabilités}
\begin{definition}[Loi de probabilités]
Définir la \textcolor{red}{loi de probabilité} d'une expérience aléatoire dont l'univers est fini, c'est associer à chaque issue possible un nombre entre 0 et 1 qui représente les chances ou les risques que l'expérience aboutisse à ce résultat.\\
La somme des probabilités de chacune des issues possibles vaut 1.
\end{definition}

\begin{definition}
Si chaque événement élémentaire d'une expérience aléatoire a la même probabilité, on dit qu'il y a \textcolor{red}{équiprobabilité} ou que la loi est \textcolor{red}{équirépartie}.
\end{definition}

\begin{propriete}
Si la loi est équirépartie :
\[p(A)=\dfrac{\text{nombre d'issues dans $A$}}{\text{nombre total d'issues dans $\Omega$}}\]
\end{propriete}

\begin{propriete}
\[p(\Omega)=1\]
\[p(\varnothing)=0\]
\[p(\overline{A})=1-p(A)\]
\[p(A\cup B)=p(A)+p(B)-p(A\cap B)\]
\end{propriete}
\end{document}