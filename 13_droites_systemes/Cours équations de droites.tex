\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother


\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}

\newtheorem{preuve}{Preuve}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Équations de droites, systèmes d'équations}
\author{J.B.S. 2\up{nde}}
\date{}

\begin{document}

\maketitle

\section*{Introduction}

Le cours d'introduction aux fonctions, ainsi que les éléments de cours
vus en classe de 3\no{} ont permis d'aborder la notion d'équation de
droite. En effet, on y rappellé que les fonctions affines, dont
l'expression est $f(x) = ax + b$ ont pour représentation graphique une
droite.

Il est légitime de se poser les questions suivantes :
\begin{itemize}
  \item quelle définition donner désormais à une droite ?
  \item existe-t-il d'autres fonctions dont la représentation graphique
    est une droite ?
  \item toutes les droites peuvent-elles se mettre sous la forme d'une
    telle équation ?
  \item comment utiliser cette éventuelle équation de droite pour en
    déduire des calculs démontrant des faits géométriques
\end{itemize}

Ce chapitre tente de répondre à ces questions là, et en profitera pour
faire le lien avec l'approche vectorielle de la géométrie du chapitre
précédent.

\section{Une première représentation des droites}

On peut établir une correspondance entre l'expression d'une fonction
affine et sa représentation graphique, c'est à dire une droite. On peut
proposer la définition suivante, de façon temporaire :
\begin{definition*}
  Une droite est l'ensemble des points $M$ du plan de coordonnées
  $(x;y)$ qui vérifient une équation appellée équation de la droite.

  On peut noter $\mathscr{D} = \left\lbrace (x,y)\in\R^2 \mid
  \text{$x$ et $y$ vérifient une équation de droite} \right\rbrace$
\end{definition*}

Explorons des ensembles de points permettant d'obtenir des droites :
\begin{itemize}
  \item l'ensemble des points $M$ du plan tels que $y = 3x + 2$ forme
    une droite \\ $\mathscr{D} = \left\lbrace (x,y)\in\R^2 \mid y = 3x +2
    \right\rbrace$ ;
  \item l'ensemble des points $M$ du plan tels que $y = -2$ forme une
    droite \\ $\mathscr{D} = \left\lbrace (x,y)\in\R^2 \mid \forall x \in
    \R, y = -2 \right\rbrace$ ;
   \item l'ensemble des points $M$ du plan tels que $x = 0$ forme une
    droite \\ $\mathscr{D} = \left\lbrace (x,y)\in\R^2 \mid \forall y \in
    \R, x = 0 \right\rbrace$.
\end{itemize}

Il est clair que le deuxième cas de figure est un cas particulier du
premier, en posant $a = 0$. En revanche, l'équation de droite du premier
cas de figure ne permet pas de passer au troisième cas de figure. Pour
cela, on pose la définition suivante :

\begin{definition}[équation généralisée de droite]
  On appelle équation généralisée de droite une équation de la forme $ax
  + by + c = 0$
\end{definition}

On démontre qu'une telle équation permet de décrire les 3 cas de figures
précédents.

\section{Intérêt pratique}

Le premier intérêt pratique des équations généralisées de droites est la
résolution de systèmes d'équations par une méthode graphique. En effet,
le point d'intersection des deux droites est l'unique solution du
système. Cela permet aussi de donner une condition sur la résolvabilité
de tels systèmes.

Profitons en pour explorer sur un exemple la méthode de Gauss (aussi
dite par combinaison linéaire) pour la résolution des systèmes.

\[
  \left\lbrace\begin{array}{lr}
    2x + 3y = 5 & \\
    7x - 11y = 13
  \end{array}\right.
\]
\hfill$\iff$\hfill\hfill\hfill~
\[
  \left\lbrace\begin{array}{lr}
    14x + 21y = 35 & L_1 \leftarrow 7\times L_1 \\
    -14x + 22y = 26 & L_2 \leftarrow -2\times L_2
  \end{array}\right.
\]
\hfill$\iff$\hfill\hfill\hfill~
\[
  \left\lbrace\begin{array}{lr}
    14x + 21y = 35 & \\
    43y = 61 & L_2 \leftarrow L_1 + L_2
  \end{array}\right.
\]
\hfill$\iff$\hfill\hfill\hfill~
\[
  \left\lbrace\begin{array}{lr}
    14x + 21y = 35 & \\
    y = \frac{61}{43} &
  \end{array}\right.
\]
\hfill$\iff$\hfill\hfill\hfill~
\[
  \left\lbrace\begin{array}{lr}
    14x + 21y = 35 & \\
    21y = \frac{21\times 61}{43} &
  \end{array}\right.
\]
\hfill$\iff$\hfill\hfill\hfill~
\[
  \left\lbrace\begin{array}{lr}
    14x = 35 - \frac{21\times 61}{43} & L_1 \leftarrow L_1 - L_2 \\
    21y = \frac{21\times 61}{43} &
  \end{array}\right.
\]
\hfill$\iff$\hfill\hfill\hfill~
\[
  \left\lbrace\begin{array}{lr}
    x = \frac{35 - \frac{21\times 61}{43}}{14} & \\
    y = \frac{61}{43} &
  \end{array}\right.
\]

On a donc exprimé $x$ et $y$ de façon unique, en 7 étapes (sans
détailler)

On obtient par cette méthode un résultat unique de façon fiable.
Attention, il est nécessaire de noter qu'une ligne est toujours
remplacée par une combinaison linéaire d'elle-même et éventuellement
d'une autre ligne.

\end{document}
