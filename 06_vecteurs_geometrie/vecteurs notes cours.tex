\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}


\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{Lycée \textsc{LaSalle Saint-Denis}}}}
\rfoot{\footnotesize{Page \thepage/ \pageref{LastPage}}}
\rhead{}
\lhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}


\makeatletter
\renewcommand{\@evenfoot}%

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{theoreme}{Théorème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}

\title{Vecteurs}
\author{2\up{nde}}
\date{}

\begin{document}

\maketitle
\section{Le vecteur, expression d'un déplacement}

\begin{definition}
  Soit $A$ et $B$ un couple de point. On  admet qu'il existe une unique translation  qui  transforme le point $A$ en un point $B$ donné.\\
  Cette unique translation définit ainsi, le vecteur $\Vecteur{AB}$.
\end{definition}


\begin{definition}
  On définit, pour un vecteur $\Vecteur{AB}$ donné, les trois
  informations suivantes :
  \begin{itemize}
    \item \textcolor{blue}{ direction} -- c'est la droite $(AB)$ qui porte le vecteur ;
    \item \textcolor{blue}{  sens} -- pour le vecteur précédent, de $A$ vers $B$ ;
    \item \textcolor{blue}{ norme} -- c'est la « longueur » du vecteur.
  \end{itemize}
  On note cette dernière quantité $\lVert\Vecteur{AB}\rVert$.
\end{definition}

\begin{propriete}[égalité de deux vecteurs]
  Deux vecteurs sont égaux si et seulement si ils définissent le même
  déplacement
\end{propriete}
Une conséquence immédiate  :
\begin{propriete}
  Deux vecteurs sont égaux si et seulement si ils ont :
  \begin{itemize}
    \item même direction ;
    \item même sens ;
    \item même norme.
  \end{itemize}
\end{propriete}

Interprétation géométrique de l'égalité de deux vecteurs :
\begin{propriete}
  Deux vecteurs $\Vecteur{AB}$ et $\Vecteur{CD}$ sont \textcolor{blue}{ égaux } si et
  seulement si $ABDC$ est un \textcolor{blue}{ parallélogramme}.
\end{propriete}

\begin{center}
  \begin{tikzpicture}[yscale=0.5]
    \draw[->] (0,0) node[below left] {$A$} -- (7,4) node[right] {$B$} ;
    \draw[->,xshift=-50,yshift=30] (0,0) node[left] {$C$} -- (7,4)
    node[above right] {$D$} ;
    \draw[dashed] (0,0) -- (0-50pt,0+30pt) ;
    \draw[dashed] (7,4) -- (7cm-50pt,4cm+30pt) ;
  \end{tikzpicture}
\end{center}

\begin{definition}[notation du vecteur opposé]
  Le vecteur opposé au vecteur $\Vecteur{AB}$ est noté $\Vecteur{BA} = -\Vecteur{AB}$
\end{definition}
\begin{remarque} 
Deux vecteurs \textcolor{blue}{opposés} ont même direction, même longueur mais sont de sens contraires.
\end{remarque}
\pagebreak
\begin{propriete}[caractérisation du milieu d'un segment]
  $I$ est le milieu du segment $AB$ si et seulement si $\Vecteur{AI} =
  \Vecteur{IB}$
\end{propriete}

\begin{center}
  \begin{tikzpicture}[yscale=0.7]
    \draw[-stealth] (0,0) node[below left] {$A$} -- (3.5,2) node[below right] {$I$};
    \draw[->]    (3.5,2) --(7,4) node[right] {$B$} ;
  \end{tikzpicture}
\end{center}
\section{Opérations sur les vecteurs}

\begin{definition}[vecteur nul]
  On admet l'existence d'un vecteur \textcolor{blue}{nul}, correspondant à la translation
  qui déplace le point $A$ sur lui-même.

  On note $\Vecteur{AA} = \vec{0}$.
\end{definition}
\subsection{Somme de vecteurs}

\begin{definition}[somme de vecteurs]
  Soit $\vec{u}$ et $\vec{v}$ deux vecteurs du plan. On définit la \textcolor{blue}{somme}
  de deux vecteurs comme le déplacement équivalent à ces deux
  déplacements successifs.

  Géométriquement, la construction est comme il suit :
  \begin{center}
    \begin{tikzpicture}
      \draw [->] (0,0) -- (2,1) ;
      \draw [->,shift={(-0.5,-1)}] (0,0) -- (3,-2) ;
      \draw [->,dashed] (0,0) -- (3,-2) ;
      \draw [->,dashed,shift={(2,1)}] (0,0) -- (3,-2) ;
      \draw [->,thick] (0,0) -- (2+3,1+-2) ;
      \draw (1,0.5) node [above] {$\vec{u}$} ;
      \draw [shift={(-0.5,-1)}] (1.5,-1) node [above] {$\vec{v}$} ;
      \draw ({(2+3)/2},{(1+-2)/2}) node [above] {$\vec{u}+\vec{v}$} ;
    \end{tikzpicture}
  \end{center}
\end{definition}

Le protocole de construction est le suivant :
\begin{itemize}
  \item tracer la droite parallèle à la droite portant $\vec{v}$ passant
    par l'extrémité du vecteur $\vec{u}$ ;
  \item reporter la longueur (norme) du vecteur $\vec{v}$ sur cette
    droite ;
  \item le point ainsi obtenu est l'extrémité d'arrivée du vecteur somme
    $\vec{u}+\vec{v}$ ;
  \item le vecteur somme est le vecteur qui part du point de départ de
    $\vec{u}$ et qui va à l'extrémité obtenue à l'étape précédente.
\end{itemize}

$\vec{u} + \vec{v} = \vec{v} +\vec{u}$.

$\vec{u} + (\vec{v} +
\vec{w} ) = (\vec{u} + \vec{v} ) + \vec{w}$.
\pagebreak
\begin{propriete}[relation de Chasles]
  Pour tout vecteur $\Vecteur{AB}$ et pour tout point $C$, on a
  l'égalité suivante : \[ \Vecteur{AB} = \Vecteur{AC} + \Vecteur{CB} \]
\end{propriete}

\begin{center}
  \begin{tikzpicture}
    \draw (0,0) node [below] {$A$} ;
    \draw (2,3) node [right] {$B$} ;
    \draw (4,2) node [above right] {$C$} ;
    \draw (-3,0) node [left] {$C'$} ;

    \draw [->] (0,0) -- (2,3) ;
    \draw [->] (0,0) -- (4,2) ;
    \draw [->] (4,2) -- (2,3) ;

    \draw [->] (0,0) -- (-3,0) ;
    \draw [->] (-3,0) -- (2,3) ;
  \end{tikzpicture}
\end{center}

\subsection{Différence de vecteurs}


Ainsi, $\vec{u} - \vec{v} = \vec{u} + (- \vec{v}) $.

\begin{center}
  \begin{tikzpicture}
    \draw [->] (3,-2) -- (5,-1) ;
    \draw [->,shift={(-0.5,-1)}] (0,0) -- (3,-2) ;
    \draw [->,dashed] (0,0) -- (3,-2) ;
    \draw [->,dashed,shift={(2,1)}] (0,0) -- (3,-2) ;
    \draw [->,thick,shift={(3,-2)}] (0,0) -- (2-3,1--2) ;
    \draw (4,-1.5) node [above] {$\vec{u}$} ;
    \draw [shift={(-0.5,-1)}] (1.5,-1) node [above] {$\vec{v}$} ;
    \draw ({(2+3)/2},{(1+-2)/2}) node [above] {$\vec{u}-\vec{v}$} ;
  \end{tikzpicture}
\end{center}
\subsection{Produit d'un vecteur par un nombre réel}

Commençons par le produit d'un vecteur par un nombre entier positif : il
s'agit simplement, de reporter, dans la même direction et dans le même
sens la longueur (ou norme du vecteur).
\begin{center}
  \begin{tikzpicture}
    \draw [thick,->, gray] (0,0) -- (2,1) ;
    \draw [->,shift={(2,1)},red, very thick] (0,0) -- (2,1) ;
  \end{tikzpicture}
\end{center}

On peut, assez facilement, définir le produit d'un vecteur par un entier
négatif : on reporte le vecteur autant de fois que nécessaire, mais dans
la direction opposée.
\begin{center}
  \begin{tikzpicture}
    \draw [thick,->, gray] (0,0) -- (2,1) ;
    \draw [->,shift={(0,0)}, red] (0,0) -- (-2*2,-1*2) ;
  \end{tikzpicture}
\end{center}

\pagebreak
\begin{definition}[produit d'un vecteur par un réel]
  Soit $\vec{u}$ un vecteur du plan et $k$ un nombre réel.
  \begin{itemize}
    \item si $k>0$, $k\vec{u}$ est un vecteur de même direction et même
      sens que $\vec{u}$, et de norme $k\lVert\vec{u}\rVert$ ;
    \item si $k<0$, $k\vec{u}$ est un vecteur de même direction que
      $\vec{u}$, de sens opposé à  $\vec{u}$, et de norme
      $-k\lVert\vec{u}\rVert$ ;
    \item si $k=0$, $k\vec{u} = \vec{0}$
  \end{itemize}
\end{definition}

\begin{propriete}[distributivité des scalaires et des vecteurs]
  \begin{itemize}
    \item $k,k'$ des nombres réels et $\vec{u}$ un vecteur,
      $(k+k')\vec{u} = k\vec{u} + k'\vec{v}$
    \item $k$ est un réel  et $\vec{u}$ et $\vec{v}$ deux
      vecteurs, $k(\vec{u} + \vec{v}) = k\vec{u} + k\vec{v}$.
  \end{itemize}
\end{propriete}
\section{Colinéarité}

\begin{definition}[Colinéarité]
  On dit que deux vecteurs $\vec{u}$ et $\vec{v}$ sont colinéaires si
  et seulement si il existe $k\in\ R^*$ tel que $\vec{v} =
  k\vec{u}$ ou $\vec{u} = k\vec{v}$.
\end{definition}

\begin{propriete}[une autre formulation de la colinéarité]
  La définition précédente est équivalente à $\vec{u}$ et $\vec{v}$ sont
  colinéaires s'il existe un couple de nombres réels différents de
  $(0,0)$ tel que $a\vec{u} + b\vec{v} = \vec{0}$.
\end{propriete}


\begin{propriete}[colinéarité en terme de droites]
  Deux vecteurs sont colinéaires si et seulement s'ils ont même
  direction (ou sont portés par des droites parallèles).
\end{propriete}
\section{Repérage}
\begin{definition}
 Soient $O$ un point du plan et $\vec{i}$ et $\vec{j}$ deux vecteurs de ce plan de directions diff\'erentes (non colin\'eaires), alors $(O;\vec{i};\vec{j})$ est appel\'e \emph{rep\`ere} du plan.
\end{definition}
\begin{remarque}
 $O$ est appel\'ee \emph{origine} du rep\`ere et le couple $\left(\vec{i},\vec{j}\right)$ est appel\'e \emph{base} du rep\`ere.
\end{remarque}
\begin{definition}
 Soit un rep\`ere $(O;\vec{i};\vec{j})$ du plan.
\begin{itemize}
 \item Si les directions de $\vec{i}$ et $\vec{j}$ sont orthogonales, le rep\`ere est dit \emph{orthogonal}.
 \item Si les normes de $\vec{i}$ et $\vec{j}$  sont \'egales \`a 1, le rep\`ere est dit \emph{norm\'e}.
 \item Si les directions de $\vec{i}$ et $\vec{j}$  sont orthogonales et que les normes de $\vec{i}$ et de $\vec{j}$  sont \'egales \`a 1, le rep\`ere est dit \emph{orthonorm\'e}.
 \item Sinon, le rep\`ere est dit \emph{quelconque}.
\end{itemize}
\end{definition}
\begin{multicols}{3}
\begin{tikzpicture}
\draw[gray,-stealth](0,0)--(0,3)node[right]{$y$};
\draw[gray, -stealth] (0,0)--(3,0)node[right]{$x$};
\draw[-stealth, very thick] (0,0)--(0,1)node[midway,left]{$\vec{j}$};
\draw[-stealth, very thick] (0,0)--(2,0)node[below, midway]{$\vec{i}$};
\end{tikzpicture}

\emph{repère orthogonal}

\begin{tikzpicture}
\draw[-stealth, very thick] (0,0)--(0.7,0.7)node[midway,above]{$\vec{j}$};
\draw[gray,-stealth] (0,0)--(3,3)node[left]{$y$};
\draw[-stealth, very thick] (0,0)--(1,0)node[below, midway]{$\vec{i}$};
\draw[gray,-stealth] (0,0)--(3,0) node[right]{$x$};
\end{tikzpicture}

\emph{repère normé}

\begin{tikzpicture}
\draw[gray,-stealth](0,0)--(0,3)node[right]{$y$};
\draw[gray, -stealth] (0,0)--(3,0)node[right]{$x$};
\draw[-stealth, very thick] (0,0)--(0,1)node[midway,left]{$\vec{j}$};
\draw[-stealth, very thick] (0,0)--(1,0)node[below, midway]{$\vec{i}$};
\end{tikzpicture}

\emph{repère orthonormal}
\end{multicols}
\subsection{Coordonnées dans une base}

\begin{definition}[coordonnées d'un vecteur dans une base]
  Soit $\vec{w}$ un vecteur dont l'expression dans la base $(\vec{u} ;
  \vec{v})$ est $\vec{w} = \alpha\vec{u} + \beta\vec{v}$.

  On dit alors que $\alpha$ et $\beta$ sont les coordonnées de $\vec{w}$
  dans la base $(\vec{u} ;\vec{v})$ et on note \[ \vec{w} 
  \left(\begin{array}{c}\alpha \\ \beta\end{array}\right) \] s'il n'y a
  pas d’ambiguïté. 
\end{definition}

\begin{propriete}
Le plan est muni d'un repère. Soient $\Vecteur{u}\,\left(x\,;\,y\right)$ et $\Vecteur{v}\,\left(x'\,;\,y'\right)$ deux vecteurs et $k$ un nombre.
\begin{itemize}
 \item $\Vecteur{u}=\Vecteur{v} \Leftrightarrow x=x' et y=y'$.
  \item Le vecteur $\Vecteur{u}+\Vecteur{v}$ a pour coordonnées $\left( x+x';y+y'\right) $.
  \item Le vecteur $k\Vecteur{u}$ a pour coordonnées $\left( kx;ky\right) $.
\end{itemize}
\end{propriete}
\begin{exemple}
Dans un repère $(O;\vec{i};\vec{j})$, on a $\vec{u}   \left(\begin{array}{c}8 \\ -5\end{array}\right)$ et $\vec{v}  \left(\begin{array}{c}-3 \\12\end{array}\right)$
\begin{enumerate}
\item Les vecteurs sont-ils égaux?
\item Calculer les coordonnées du vecteur $\Vecteur{u}+\Vecteur{v}$.
\item Calculer les coordonnées du vecteur $2\Vecteur{u}$.
\item Calculer les coordonnées du vecteur $-0,4\Vecteur{v}$.
\end{enumerate}
\ligne{10}
\end{exemple}
\begin{definition}
 Le plan \'etant muni d'un rep\`ere $(O;\Vecteur{i};\Vecteur{j})$, on appelle \emph{coordonn\'ees} du point $M$ le couple $(x;y)$ tel que $\Vecteur{OM}=x\Vecteur{i}+y\Vecteur{j}$, $x$ \'etant appel\'e \emph{abscisse} de $M$ et $y$ \'etant appel\'e \emph{ordonn\'ee} de $M$.
\end{definition}
Les coordonn\'ees du point $M$ sont donc les coordonn\'ees du vecteur $\Vecteur{OM}$. Cela implique qu'elles d\'ependent de l'origine du rep\`ere.
\begin{propriete}[Coordonnées d'un vecteur]
 Soient $A\,(x_A;y_A)$ et $B\,(x_B;y_B)$ deux points du plan. \\Alors les coordonn\'ees du vecteur $\Vecteur{AB}$ sont $
  \left(\begin{array}{c}x_B-x_A \\ y_B-y_A\end{array}\right) $.
\end{propriete}
\begin{exemple}
Dans un repère orthogonal $(O;I;J)$, on a les points $A(-2;3)$ et $B(4;-1)$\\
\ligne{5}
%Coordonnées du vecteur $\vec{AB}$ sont $   \left(\begin{array}{c}4-(-2) \\ -1-3\end{array}\right) $\\

%$\Vecteur{AB}  \left(\begin{array}{c}6 \\ -4\end{array}\right) $
\end{exemple}

\begin{propriete}[Coordonnées du milieu d'un segment]
 Soit $P$ un plan muni d'un rep\`ere quelconque.\\
 Soit $A(x_A;y_A)$ et $B(x_B;y_B)$ et $I(x_I;y_I)$ milieu de $[AB]$. \\
Alors $\qquad x_I=\dfrac{x_A+x_B}{2}$ et $\qquad  y_I=\dfrac{y_A+y_B}{2}$
\end{propriete}

\begin{exemple}
Dans un repère orthogonal $(O;I;J)$, on a les points $A(-2;3)$ et $B(4;-1)$\\
\ligne{5}
%abscisse du milieu du segment $[AB]$ :$x_I=\dfrac{-2+4}{2}$\\
%$x_I=1$\\
%ordonnée du milieu du segment $[AB]$ : $y_I=\dfrac{3-1}{2}$\\
%$y_I=1$\\
%Le point $I$ a pour coordonnées $(1;1)$
\end{exemple}
\begin{propriete}[Norme d'un vecteur]
 Soit $P$ un plan muni d'un rep\`ere \underline{\textbf{orthonorm\'e}}.\\
 Soient $A$ et $B$ deux points du plan $P$ de coordonn\'ees respectives $(x_A;y_A)$ et $(x_B;y_B)$.\\
 Alors la distance $AB$ est donn\'ee par :
 \[AB=\sqrt{(x_B-x_A)^2+(y_B-y_A)^2}\]
\end{propriete}
\begin{exemple}
Dans un repère orthogonal $(O;I;J)$, on a les points $A(-2;3)$ et $B(4;-1)$\\
%La norme du vecteur $\norme{\Vecteur{AB}}$ \\
\ligne{10}
%est : $\sqrt{(4-(-2))^2+(-1-3)^2}$\\
%$\norme{\Vecteur{AB}}=\sqrt{(6)^2+(-4)^2}$\\
%$\norme{\Vecteur{AB}}=\sqrt{36+16}$\\
%$\norme{\Vecteur{AB}}=\sqrt{52}$\\
%$\norme{\Vecteur{AB}}=2\sqrt{13}$
\end{exemple}



\subsection{Colinéarité}

\begin{definition}[Colinéarité]
  On dit que deux vecteurs $\vec{u}$ et $\vec{v}$ sont colinéaires si
  et seulement si il existe $k\in\mathbf{R^*}$ tel que $\vec{v} =
  k\vec{u}$ ou $\vec{u} = k\vec{v}$.
\end{definition}

\begin{propriete}[une autre formulation de la colinéarité]
Deux vecteurs non nuls $\Vecteur{AB}$ et $\Vecteur{CD}$ sont \emph{colinéaires}
\begin{itemize}
\item[$\Leftrightarrow$] les droites $(AB)$ et $(CD)$ sont parallèles. 
\item[$\Leftrightarrow$] les coordonnées de $\Vecteur{AB}$ et $\Vecteur{CD}$ sont proportionnelles
\item[$\Leftrightarrow$]  $x_{\Vecteur{AB}} \times y_{\Vecteur{CD}}=y_{\Vecteur{AB}} \times x_{\Vecteur{CD}}$
\item[$\Leftrightarrow$] il existe $k\in\R$ tel que $\Vecteur{AB}=k\,\Vecteur{CD}$ (ou $k\,\Vecteur{AB}=\Vecteur{CD}$)
\end{itemize}
\end{propriete}

\begin{exemple}
 Soit $ABC$ un triangle rectangle en $A$.
    \begin{itemize}
    	\item $\Vecteur{AD}=\Vecteur{BA}$
        		\item $\Vecteur{CE}=\Vecteur{CB}+\Vecteur{CD}$
    \end{itemize}
 Quelle est la nature du quadrilatère $BCDE$ ? Justifier.\\
 \ligne{8}
\end{exemple}

\begin{exemple}
Soient $A(0;1)$ ,$B(\sqrt 2;2)$ et $C(2;\sqrt 2+1)$  . Montrer que les points sont alignés?
\begin{enumerate}
\item Calculer les coordonnées du vecteur $\Vecteur{AB}$\\
\ligne{4}
\item Calculer les coordonnées du vecteur $\Vecteur{AC}$\\
\ligne{4}
\item Montrer que les vecteurs $\Vecteur{AB}$ et $\Vecteur{AC}$ sont colinéaires.\\
\ligne{3}
\item En déduire que les points $A,B$ et $C$ sont alignés.\\
\ligne{4}
\end{enumerate}
\end{exemple}

\begin{propriete}[colinéarité en terme de droites]
  Deux vecteurs sont colinéaires
  \begin{itemize}
\item[$\Leftrightarrow$] les vecteurs ont même direction
\item[$\Leftrightarrow$] les vecteurs sont portés par des droites parallèles.
\end{itemize}
\end{propriete}

\begin{propriete}
$I$ est le milieu du segment$[AB]$
\begin{itemize}
\item[$\Leftrightarrow$] $\Vecteur{AI}=\Vecteur{IB}$
\item[$\Leftrightarrow$] $\Vecteur{AB}=2\Vecteur{AI}$\\

\item[$\Leftrightarrow$] $\Vecteur{AI}=\dfrac{1}{2}\Vecteur{AB}$
\end{itemize}
\end{propriete}

\begin{exemple}
Soit le triangle $ABC$, $I$ est le milieu du segment $[AB]$ et J est le milieu du segment $[AC]$.\\
 Montrer que $\Vecteur{IJ}=\dfrac{1}{2}\Vecteur{BC}$.\\
 \ligne{13}
\end{exemple}
\end{document}
