#!/usr/bin/python3

import csv
import requests
import json
import datetime
import sys

from base64 import urlsafe_b64encode as b64encode

def connect(username, password):
    """
    Une fonction qui se connecte
    """
    auth = {"identifiant": username, "motdepasse": password}
    r = requests.post(
        'https://apip.ecoledirecte.com/v3/login.awp',
        data = f'data={json.dumps(auth)}'
        )
    return json.loads(r.text)['token']


def get_edt(token, date):
    date_next_day = datetime.datetime.strftime((datetime.datetime.fromisoformat(date) + datetime.timedelta(days=1)),'%Y-%m-%d')
    data = f'data={{"token":"{token}"}}'
    r = requests.post(
        f'https://apip.ecoledirecte.com/v3/cahierdetexte/loadslots/{date}/{date}.awp?verbe=get',
        data=data
    )
    return json.loads(r.text)['data']


def get_classe_seance(token, date, classe):
    for cours in get_edt(token, date):
        if cours['entityCode'] == classe:
            return cours


def put_seance(token, date, classe, contenu):
    seance_info = get_classe_seance(token, date, classe)
    seance_info['token'] = token
    if 'seance' not in seance_info.keys():
        seance_info['seance'] = dict()
    seance_info['seance']['modeEdit'] = "True"
    seance_info['seance']['contenu'] = b64encode(bytes(contenu,'utf-8')).decode('utf-8')
    data = f'data={json.dumps(seance_info)}'
    context = requests.post(
        f'https://apip.ecoledirecte.com/v3/cahierdetexte/seance/{classe}/{seance_info["matiereCode"]}/{date}.awp?verbe=put',
        data=data
    )
    return context

def attach_document(document, context):
    document = document.split('/')[-1]
    idContexte = json.loads(context.text)["data"]['idCDT']
    token = json.loads(context.text)["token"]
    with open(document,'rb') as f:
        data = f.read()
    boundary='---------------------------23681658131424797482946919298'
    headers = json.loads(f"""{{"Content-Type": "multipart/form-data; boundary={boundary}"}}""")
    magic = f"""{{"token":"{token}","idContexte":"{idContexte}","side":"seance"}}"""
    data_binary = f"""{boundary}
Content-Disposition: form-data; name="data"\r\n\r\n{magic}
{boundary}
Content-Disposition: form-data; name="file"; filename="{document}"
Content-Type: application/pdf
{data.decode('iso-8859-1')}
\r\n{boundary}--\r\n"""
    r = requests.post('https://apip.ecoledirecte.com/v3/televersement.awp?verbe=post&mode=CDT',
        headers=headers,
        data=data_binary
    )
    return magic,r



if __name__ == '__main__':
    username, password= tuple(sys.argv[1:])

    token = connect(username, password)
    with open('compile.csv','r') as fp:
        fichiers = csv.DictReader(fp, delimiter=';')
        for seance in fichiers:
            context = put_seance(token, seance['dates'], seance['classe'], seance['message'])
            documents = seance['fichiers'].split(' ')
            for document in documents:
                r = attach_document(document.replace('.tex','.pdf'), context)

