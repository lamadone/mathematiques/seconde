\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{trees}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{break}
\newtheorem{exemple}{Exemple}
\newtheorem{proposition}{Proposition}
\newtheorem{propriete}{Propriété}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\newcommand{\R}{\mathbf{R}}

\title{Probabilités}
\author{J.B.S. 2\up{nde}}
\date{}

\begin{document}

\maketitle

\hfill\textit{Mesurer le hasard, quelle hasardeuse entreprise}\hfill\hfill

Les théories relatives aux probabilités sont assez anciennes, bien que
revisitées au début du XX\ieme{} siècle, grace aux avancés de Lebesgue
et de Kolmogorov. Nous n'entrerons pas dans les détails ici à ce niveau,
et nous explorerons plutôt les probabilités à la manière de Fermat et de
Pascal, pionniers historiques de cette branche des mathématiques.

\section{Un peu de vocabulaire}
Nous n'allons pas donner une liste ici de mots de vocabulaire, mais nous
allons plutôt l'introduire au fur et à mesure de notre propos.

Tout d'abord, pour l'instant, on ne cherchera à mesurer le hasard que
lorsque les \emph{issues} de notre \emph{expérience aléatoire} sont en
nombre fini et constituent donc un \emph{univers des possibles} qui est
un sous ensemble d'un ensemble souvent plus vaste.

On parle aussi d'\emph{événements} pour désigner les parties de
l'univers et même d'\emph{événements élementaires} lorsque ceux-ci sont
réduits à un point (noté par le singleton $\{a\}$).

Il est parfois plus facile de raisonner sur l'\emph{événement contraire}
d'un évenement pour obtenir un probabilité. Si on note $A$ un événement,
on note $\overline{A}$ son événement contraire.

S'intéresser à la probabilité que deux événéments se réalisent
«simultanément» se note, dans le langage ensembliste l'intersection et
se note $A\cap B$. On dit aussi \emph{$A$ et $B$}. Si on ne s'intéresse
qu'à l'une des deux possibilités, on parle de l'union, notée $A\cup B$.
On dit aussi \emph{$A$ ou $B$}.

\section{Axiomatisation incomplète et naïve}
Cette axiomatique (ou suite de proposition) est à la fois incomplète,
naïve et non minimale, au sens ou certaines des propositions peuvent se
déduire des autres. Néanmoins, elle nous sera fort utile pour débuter en
probabilité.

\begin{proposition}
  $\mathcal{P}(\emptyset)=0$
\end{proposition}

\begin{proposition}
  $\mathcal{P}(\overline{A}) = 1 - \mathcal{P}(A)$
\end{proposition}

La proposition suivante découle facilement des précédentes.
\begin{proposition}
  Si $\Omega$ est l'univers des posibles, $\mathcal{P}(\Omega) = 1$
\end{proposition}

\pagebreak
Enfin, on peut noter les deux propositions suivantes :
\begin{proposition}
  \begin{itemize}
    \item si $A$ et $B$ sont disjoints, $\mathcal{P}(A\cup B) =
      \mathcal{P}(A) + \mathcal{P}(B)$ ;
    \item sinon, $\mathcal{P}(A\cup B) = \mathcal{P}(A) + \mathcal{P}(B)
      - \mathcal{P}(A\cap B)$.
  \end{itemize}
\end{proposition}

\section{Calcul effectif des probabilités}

\subsection{Avec un arbre de probabilités}

Exemple : 29 p 196

On place les probabilités sur les branches de l'arbre de probabilités.

La probabilité au bout d'une suite de branches s'obtient par
multiplication des probabilités des branches parcourues.

La somme des branches d'un même nœud de départ vaut 1.

\begin{center}
  \begin{tikzpicture}[level distance=2cm, sibling distance=4cm]
    \tikzset{level 2/.style={sibling distance=12ex}}
    \tikzset{level 1/.style={sibling distance=24ex}}
    \node[grow=down] {}
    child { node {F}
      child { node {V}
        edge from parent node[above left] {$\frac12$}
      }
      child { node {R}
        edge from parent node[above left] {$\frac12$}
      }
      edge from parent node[above left] {$\frac13$}
    }
    child { node {R}
      child { node {V} }
      child { node {F} }
      edge from parent node[above left] {$\frac13$}
    }
    child { node {V}
      child { node {F} }
      child { node {R} }
      edge from parent node[above left] {$\frac13$}
    }
    ;
  \end{tikzpicture}
\end{center}

On a, par exemple $\mathcal{P}(F\to V\to R) = \frac13\times \frac12 =
\frac16$.

\subsection{Avec un tableau}

Exemple : 22 p 195

L'intersection de deux événements est l'intersection d'une ligne et
d'une colonne.

L'union de deux évenements est la somme lignes (ou des colonnes) des
événements correspondants. Dans le cas d'un tableau, on appelle cela la
«formule des probabilités totales».

\begin{center}
  \begin{tabular}{|*{4}{p{5em}|}} \hline
    & Anglais & Allemand & Espagnol \\ \hline
    Garçons & 8 & 3 & 4 \\ \hline
    Filles & 10 & 4 & 6 \\ \hline
  \end{tabular}
\end{center}

On a, par exemple $\mathcal{P}(\lbrace\text{«Filles qui étudient
l'anglais»}\rbrace) = \frac{10}{8+3+4+10+4+6} = \frac{10}{35} =
\frac{2}{7}$.


\end{document}
