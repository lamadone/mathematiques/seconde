\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{theoreme}{Théorème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}

\title{Vecteurs}
\author{2\up{nde}}
\date{}

\begin{document}

\maketitle

\section{Repérage}
\begin{definition}
 Soient $O$ un point du plan et $\vec{i}$ et $\vec{j}$ deux vecteurs de ce plan de directions diff\'erentes (non colin\'eaires), alors $(O;\vec{i};\vec{j})$ est appel\'e \emph{rep\`ere} du plan.
\end{definition}
\begin{remarque}
 $O$ est appel\'ee \emph{origine} du rep\`ere et le couple $\left(\vec{i},\vec{j}\right)$ est appel\'e \emph{base} du rep\`ere.
\end{remarque}
\begin{definition}
 Soit un rep\`ere $(O;\vec{i};\vec{j})$ du plan.
\begin{itemize}
 \item Si les directions de $\vec{i}$ et $\vec{j}$ sont orthogonales, le rep\`ere est dit \emph{orthogonal}.
 \item Si les normes de $\vec{i}$ et $\vec{j}$  sont \'egales \`a 1, le rep\`ere est dit \emph{norm\'e}.
 \item Si les directions de $\vec{i}$ et $\vec{j}$  sont orthogonales et que les normes de $\vec{i}$ et de $\vec{j}$  sont \'egales \`a 1, le rep\`ere est dit \emph{orthonorm\'e}.
 \item Sinon, le rep\`ere est dit \emph{quelconque}.
\end{itemize}
\end{definition}
\begin{multicols}{3}
\begin{tikzpicture}
\draw[gray,-stealth](0,0)--(0,3)node[right]{$y$};
\draw[gray, -stealth] (0,0)--(3,0)node[right]{$x$};
\draw[-stealth, very thick] (0,0)--(0,1)node[midway,left]{$\vec{j}$};
\draw[-stealth, very thick] (0,0)--(2,0)node[below, midway]{$\vec{i}$};
\end{tikzpicture}

\emph{repère orthogonal}\\

\begin{tikzpicture}
\draw[-stealth, very thick] (0,0)--(0.7,0.7)node[midway,above]{$\vec{j}$};
\draw[gray,-stealth] (0,0)--(3,3)node[left]{$y$};
\draw[-stealth, very thick] (0,0)--(1,0)node[below, midway]{$\vec{i}$};
\draw[gray,-stealth] (0,0)--(3,0) node[right]{$x$};
\end{tikzpicture}

\emph{repère normé}\\

\begin{tikzpicture}
\draw[gray,-stealth](0,0)--(0,3)node[right]{$y$};
\draw[gray, -stealth] (0,0)--(3,0)node[right]{$x$};
\draw[-stealth, very thick] (0,0)--(0,1)node[midway,left]{$\vec{j}$};
\draw[-stealth, very thick] (0,0)--(1,0)node[below, midway]{$\vec{i}$};
\end{tikzpicture}

\emph{repère orthonormal}
\end{multicols}

\begin{definition}[coordonnées d'un vecteur dans une base]
  Soit $\vec{w}$ un vecteur dont l'expression dans la base $(\vec{u} ;
  \vec{v})$ est $\vec{w} = \alpha\vec{u} + \beta\vec{v}$.

  On dit alors que $\alpha$ et $\beta$ sont les coordonnées de $\vec{w}$
  dans la base $(\vec{u} ;\vec{v})$ \\et on note $\vec{w} =
  \left(\begin{array}{c}\alpha \\ \beta\end{array}\right) $ s'il n'y a
  pas d’ambiguïté. 
\end{definition}

\begin{propriete}
Le plan est muni d'un repère. Soient $\Vecteur{u}\,\left(x\,;\,y\right)$ et $\Vecteur{v}\,\left(x'\,;\,y'\right)$ deux vecteurs et $k$ un nombre.
\begin{itemize}
 \item $\Vecteur{u}=\Vecteur{v} \Leftrightarrow x=x' et y=y'$.
  \item Le vecteur $\Vecteur{u}+\Vecteur{v}$ a pour coordonnées $\left( x+x';y+y'\right) $.
  \item Le vecteur $k\Vecteur{u}$ a pour coordonnées $\left( kx;ky\right) $.
\end{itemize}
\end{propriete}

\begin{definition}
 Le plan \'etant muni d'un rep\`ere $(O;\Vecteur{i};\Vecteur{j})$, on appelle \emph{coordonn\'ees} du point $M$ le couple $(x;y)$ tel que $\Vecteur{OM}=x\Vecteur{i}+y\Vecteur{j}$, $x$ \'etant appel\'e \emph{abscisse} de $M$ et $y$ \'etant appel\'e \emph{ordonn\'ee} de $M$.
\end{definition}

Les coordonn\'ees du point $M$ sont donc les coordonn\'ees du vecteur $\Vecteur{OM}$. Cela implique qu'elles d\'ependent de l'origine du rep\`ere.

\begin{propriete}[Coordonnées d'un vecteur]
 Soient $A\,(x_A;y_A)$ et $B\,(x_B;y_B)$ deux points du plan. \\Alors les coordonn\'ees du vecteur $\Vecteur{AB}$ sont $(x_B-x_A;y_B-y_A)$.
\end{propriete}

\begin{exemple}
Dans un repère orthonormé $(O;\vec{i};\vec{j})$ ,$A(3;4)$ et $B(4;4)$.
\begin{enumerate}
\item Calculer les coordonnées du vecteur $\Vecteur{AB}$\\
\ligne{3}
\item Calculer les coordonnées du vecteur $\Vecteur{OI}$\\
\ligne{3}
\item En déduire la nature du quadrilatère $OIBA$\\
\ligne{3}
\end{enumerate}
\end{exemple}

\begin{propriete}[Coordonnées du milieu d'un segment]
 Soit $P$ un plan muni d'un rep\`ere quelconque.\\
 Soit $A(x_A;y_A)$ et $B(x_B;y_B)$ et $I(x_I;y_I)$ milieu de $[AB]$. \\
Alors $\qquad x_I=\dfrac{x_A+x_B}{2}$ et $\qquad  y_I=\dfrac{y_A+y_B}{2}$
\end{propriete}

\begin{exemple}
Soient $A(2;-1)$ et $B(4;-3)$
\begin{enumerate}
\item Calculer l'abscisse du milieu du segment $[AB]$\\
\ligne{2}
\item Calculer l'ordonnée du milieu du segment $[AB]$\\
\ligne{2}
\end{enumerate}
\end{exemple}

\begin{propriete}[Norme d'un vecteur]
 Soit $P$ un plan muni d'un rep\`ere \underline{\textbf{orthonorm\'e}}.\\
 Soient $A$ et $B$ deux points du plan $P$ de coordonn\'ees respectives $(x_A;y_A)$ et $(x_B;y_B)$.\\
 Alors la distance $AB$ est donn\'ee par :
 $AB=\sqrt{(x_B-x_A)^2+(y_B-y_A)^2}$
\end{propriete}



\section{Colinéarité}

\begin{definition}[Colinéarité]
  On dit que deux vecteurs $\vec{u}$ et $\vec{v}$ sont colinéaires si
  et seulement si il existe $k\in\mathbf{R^*}$ tel que $\vec{v} =
  k\vec{u}$ ou $\vec{u} = k\vec{v}$.
\end{definition}

\begin{propriete}[une autre formulation de la colinéarité]
Deux vecteurs non nuls $\Vecteur{AB}$ et $\Vecteur{CD}$ sont \emph{colinéaires}
\begin{itemize}
\item[$\Leftrightarrow$] les droites $(AB)$ et $(CD)$ sont parallèles. 
\item[$\Leftrightarrow$] les coordonnées de $\Vecteur{AB}$ et $\Vecteur{CD}$ sont proportionnelles
\item[$\Leftrightarrow$]  $x_{\Vecteur{AB}} \times y_{\Vecteur{CD}}=y_{\Vecteur{AB}} \times x_{\Vecteur{CD}}$
\item[$\Leftrightarrow$] il existe $k\in\R$ tel que $\Vecteur{AB}=k\,\Vecteur{CD}$ (ou $k\,\Vecteur{AB}=\Vecteur{CD}$)
\end{itemize}
\end{propriete}

\begin{exemple}
 Soit $ABC$ un triangle rectangle en $A$.
    \begin{itemize}
    	\item $\Vecteur{AD}=\Vecteur{BA}$
        		\item $\Vecteur{CE}=\Vecteur{CB}+\Vecteur{CD}$
    \end{itemize}
 Quelle est la nature du quadrilatère $BCDE$ ? Justifier.\\
 \ligne{8}
\end{exemple}

\begin{exemple}
Soient $A(0;1)$ ,$B(\sqrt 2;2)$ et $C(2;\sqrt 2+1)$  . Montrer que les points sont alignés?
\begin{enumerate}
\item Calculer les coordonnées du vecteur $\Vecteur{AB}$\\
\ligne{4}
\item Calculer les coordonnées du vecteur $\Vecteur{AC}$\\
\ligne{4}
\item Montrer que les vecteurs $\Vecteur{AB}$ et $\Vecteur{AC}$ sont colinéaires.\\
\ligne{3}
\item En déduire que les points $A,B$ et $C$ sont alignés.\\
\ligne{4}
\end{enumerate}
\end{exemple}

\begin{propriete}[colinéarité en terme de droites]
  Deux vecteurs sont colinéaires
  \begin{itemize}
\item[$\Leftrightarrow$] les vecteurs ont même direction
\item[$\Leftrightarrow$] les vecteurs sont portés par des droites parallèles.
\end{itemize}
\end{propriete}

\begin{propriete}
$I$ est le milieu du segment$[AB]$
\begin{itemize}
\item[$\Leftrightarrow$] $\Vecteur{AI}=\Vecteur{IB}$
\item[$\Leftrightarrow$] $\Vecteur{AB}=2\Vecteur{AI}$\\

\item[$\Leftrightarrow$] $\Vecteur{AI}=\dfrac{1}{2}\Vecteur{AB}$
\end{itemize}
\end{propriete}

\begin{exemple}
Soit le triangle $ABC$, $I$ est le milieu du segment $[AB]$ et J est le milieu du segment $[AC]$.\\
 Montrer que $\Vecteur{IJ}=\dfrac{1}{2}\Vecteur{BC}$.\\
 \ligne{13}
\end{exemple}
\end{document}
