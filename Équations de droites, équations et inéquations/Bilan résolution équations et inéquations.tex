\documentclass[12pt,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}\usepackage{cmbright}
\usepackage{amsmath,amsfonts,amssymb}

\usepackage[a4paper,margin=2cm]{geometry}

\usepackage[np]{numprint}

\usepackage{tkz-base}
\usepackage{tkz-tab}

\usepackage{multicol}

\usepackage{babel}

\newcommand{\R}{\mathbf{R}}

\author{2\up{de} -- \bsc{Jumel}}
\title{Bilan sur la résolution de problèmes conduisant à une (in)équation}
\date{novembre 2017}

\begin{document}

\maketitle

\section{Résolution des équations et des inéquations du premier degré}

\subsection{Qu'est ce que c'est qu'une (in)équation}

On se donne les définitions suivantes :
\begin{itemize}
  \item une équation est une expression mathématique comportant une ou
    plusieurs inconnues et le symbole d'égalité «=» ;
  \item une inéquation est une expression mathématique comportant une
    inconnue et un des symboles suivants :
    \begin{itemize}
      \item «<», strictement inférieur ;
      \item «$\leq$« ou «$\leqslant$», inférieur ou égal (inégalité
        large) ;
      \item «>» strictement supérieur ;
      \item «$\geq$» ou «$\geqslant$»  supérieur ou égal (inégalité
        large.)
    \end{itemize}
\end{itemize}

On admet les règles suivantes :

\begin{itemize}
  \item on ne change pas une équation (ou une inéquation) en ajoutant la
    même quantité à droite et à gauche ;
  \item la même proposition est vraie pour la soustraction ;
  \item on ne change pas une équation lorsqu'on multiplie à droite et à
    gauche par la même quantité non nulle ;
  \item la même proposition est vraie pour la division par un nombre non
    nul ;
  \item si on multiplie (ou on divise par une quantité négative, on
    change le sens de l'inégalité, c'est à dire que $>$ devient $<$ et
    $\leqslant$ devient $\geqslant$.
\end{itemize}

\subsection{Résoudre une (in)équation du premier degré}

Un petit exemple pour fixer les idées :

Résoudre dans $\R$ l'inéquation $-3x+4<7$. On raisonne par équivalence
et on l'indique avec le symbole $\iff$ placé entre les lignes pour
indiquer le lien (On peut aussi préciser au début du calcul qu'on
raisonne par équivalences successives en l'écrivant.

\begin{multicols}{4}
\begin{center}
  \hphantom{$\iff$} $-3x+4<7$

  $\iff$\hphantom{ $-3x+4<7$}
  \columnbreak

  \hphantom{$\iff$} $-3x<7-4$

  $\iff$\hphantom{ $-3x+4<7$}
  \columnbreak

  \hphantom{$\iff$} $-3x<3$

  $\iff$\hphantom{ $-3x+4<7$}
  \columnbreak

  \hphantom{$\iff$} $x>\frac{3}{-3}$

  $\iff$\hphantom{ $-3x+4<7$}

  \hphantom{$\iff$} $x>-1$
\end{center}
\end{multicols}

Ce dernier résultat peut s'écrire sous la forme d'un intervalle :
$x\in]-1;+\infty[$.

On peut aussi regarder la représentation graphique correspondante à ce
«problème». Si on file l'exemple, on peut prendre la fonction affine $f$
qui à $x$ associe $-3x + 4 - 7$. Résoudre l'inéquation $-3x + 4 < 7$ est
equivalent à résoudre l'inéquation $f(x) < 0$, avec $f(x) = -3x -3$.

Comme c'est une fonction affine, sa représentation graphique est une
droite qui passe par le point de coordonnées $(0,-3)$ et de coefficient
directeur $-3$, c'est à dire que pour une augmentation d'une unité sur
l'axe des abscisses, le déplacement vertical sera de -3 unités pour
obtenir le point suivant.

\begin{center}
  \begin{tikzpicture}[>=latex]
    \tkzInit[xmin=-3,xmax=3,ymin=-9,ymax=9]
    \tkzGrid
    \tkzAxeXY

    \draw [very thick] plot [domain=-3:2] (\x,-3*\x-3) ;
    \draw (-1,0) node [circle,fill,black,inner sep=2pt] {} ;
    \draw (-1,0) node [above right] {$(-1,0)$} ;

    \draw [very thick,red] (-1,0) -- (3,0) ;
    \draw [red] (-1,0) node {$\mathbf{]}$} ;

    \draw [thick,blue,->] (0,-3) -- (1,-3) ;
    \draw [thick,blue] (0.5,-3) node [above] {1} ;
    \draw [thick,blue,->] (1,-3) -- (1,-6) ;
    \draw [thick,blue] (1,-4.5) node [right] {-3} ;
  \end{tikzpicture}
\end{center}

On peut donc lire l'(in)équation à 0 facilement : les points pour
lesquels l'image de leur abscisse est négative donnent la solution de
l'inéquation.

\section{Les (in)équations du second degré (en fait le cas simple :
(in)équations produits)}

En classe de Seconde générale, on ne va pas résoudre toutes les
équations du second degré mais juste quelques problèmes s'y ramenant
pour les quels on vous propose une version factorisée\footnote{Un D.M.
  éventuellement permettra de déterminer une condition et une méthode de
factorisation.} du polynôme du second degré.

\subsection{Résolution des équations du second degré en 2\up{de}}

La résolution des équations produits se fait en appliquant un principe
d'intégrité des ensembles de nombres que nous manipulons : un produit
de deux facteurs est nul si l'un (au moins) de ses facteurs est nul.

Mettons en œuvre sur un exemple.

On suppose que le problème posé se ramène à étudier $(-3x + 6)(2x - 5) =
0$. Ce produit est nul si l'un ou\footnote{au sens logique, c'est-à-dire
inclusif} l'autre des facteurs est nul.

Raisonnons par équivalences successives, en considérant que la
présentation en deux colonnes correspondent au «ou».

\begin{multicols}{2}
  \begin{center}
    $-3x + 6 = 0$

    $-3x = -6$

    $x = \frac{-6}{-3}$

    $x = 2$
    \columnbreak

    $2x - 5 = 0$

    $2x = 5$

    $x = \frac52$
  \end{center}
\end{multicols}

On peut résumer les solutions dans un ensemble $S$ (notation entre
accolades.) Avec l'exemple précédent, l'ensemble solution est $S =
\left\{2,\frac52\right\}$.

Avec cette notation, $x$ est solution de l'équation $(-3x + 6)(2x - 5) =
0$ si, et seulement si, $x\in S$.

En général, on ne donne pas cette équation sous forme factorisée, mais
sous sa forme développée : $-6x^2 + 27x -30 =0$. Pour l'instant, on ne
dispose pas d'une méthode générale permettant d'obtenir la forme
factorisée, ni même son existence, à partir de la forme développée.

\subsection{Résolution des inéquations du second degré}

Pour résoudre une inéquation se ramenant à une inéquation produit, on
utilise un tableau de signe.

On le construit de la façon suivante :
\begin{itemize}
  \item on détermine l'ensemble de définition, qui donne ainsi les
    bornes du tableau de signe ;
  \item on résout l'équation associée à l'inéquation pour classer les
    solutions dans l'ordre croissant ;
  \item on place sur deux lignes distinctes les signes précis des deux
    facteurs du produit ;
  \item on déduit, par application de la «règle des signes» la dernière
    ligne du tableau.
\end{itemize}

\begin{center}
  \begin{tikzpicture}
    \tkzTabInit{$x$/1,$2x - 5$/1,$-3x+6$/1,$-6x^2 + 27x
    -30$/1}{-1,2,\np{2.5},5}
    \tkzTabLine{,-,t,-,z,+}
    \tkzTabLine{,+,z,-,t,-}
    \tkzTabLine{,-,z,+,z,-}
  \end{tikzpicture}
\end{center}

On peut aussi prendre une version graphique, comme ci-dessous.

\begin{center}
  \begin{tikzpicture}[>=latex,xscale=2,yscale=0.7]
    \tkzInit[xmin=-1,xmax=5,ymin=-9,ymax=9]
    \tkzGrid
    \tkzAxeXY

    \draw [very thick] plot [domain=-1:5] (\x,-3*\x+6) ;
    \draw (2,0) node [circle,fill,black,inner sep=2pt] {} ;
    %\draw (2,0) node [above right] {$(-1,0)$} ;
    \draw [very thick,red] (2,0) -- (5,0) ;
    \draw [red] (2,0) node {$\mathbf{]}$} ;

    \draw [very thick] plot [domain=-1:5] (\x,2*\x-5) ;
    \draw (2.5,0) node [circle,fill,black,inner sep=2pt] {} ;
    %\draw (2.5,0) node [above right] {$(-1,0)$} ;
    \draw [very thick,red] (2.5,0) -- (-1,0) ;
    \draw [red] (2.5,0) node {$\mathbf{[}$} ;

    \draw [blue,very thick] (2.5,0) -- (2,0) ;
    \draw plot [smooth,domain=1:3] (\x,{(-3*\x + 6)*(2*\x - 5)});
  \end{tikzpicture}
\end{center}

On donne la solution de l'inéquation $(-3x + 6)(2x - 5) > 0$ sous la
forme de l'intervalle $]2;\np{2.5}[$ et de l'inéquation $(-3x + 6)(2x - 5)
\leqslant 0$ sous la forme d'une réunion d'intervalle avec le symbole
correspondant (le même qui sera utilisé en probabilité)
$[-1;2]\cup[\frac52;5]$ qui se lit $x$ appartient à l'un ou à
l'autre\footnote{Ici l'intersection de ces deux intervalles est vide.}.

\end{document}
